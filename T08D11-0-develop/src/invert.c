#include <math.h>
#include <stdio.h>
#include <stdlib.h>

void invert(double **matrix, int n, int m);
int input(double **matrix, int *n, int *m);
void output(double **matrix, int n, int m);
int transpose(double **matrix, int n, int m, double **fac);
double det(double **matrix, int n);
void getMatrixWithoutRowAndCol(double **matrix, int size, int row, int col, double **newMatrix);

int main() {
    int n1 = 0, m1 = 0;
    int *n = &n1, *m = &m1;
    int q = 1;
    char probel1, probel2;
    if ((scanf("%d%c%d%c", n, &probel1, m, &probel2) != 4) || ((*n <= 0) || (*m <= 0)) || (*n != *m) ||
        (((probel1 != '\n') && (probel1 != ' ')) || ((probel2 != ' ') && (probel2 != '\n')))) {
        printf("n/a");
        q = 0;
    }
    if (q) {
        double **matrix = malloc((*n) * (*m) * sizeof(double) + (*n) * sizeof(double *));
        double *ptr1 = (double *)(matrix + *n);
        for (int i = 0; i < *n; i++) matrix[i] = ptr1 + (*m) * i;
        if (input(matrix, n, m)) {
            if ((det(matrix, *n) < 1e-6) && (det(matrix, *n) > -1e-6)) {
                printf("n/a");
            } else {
                invert(matrix, *n, *m);
            }
            free(matrix);
        }
    }
    return 1;
}

int input(double **matrix, int *n, int *m) {
    char probel;
    int q = 1;
    for (int i = 0; i < *n; i++)
        for (int j = 0; j < *m; j++)
            if ((scanf("%lf%c", &(matrix[i][j]), &probel) != 2) || ((probel != '\n') && (probel != ' '))) {
                q = 0;
            }
    if ((!q) || (matrix == NULL)) {
        printf("n/a");
        q = 0;
    }
    return q;
}

void output(double **matrix, int n, int m) {
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
            if (j < m - 1) {
                printf("%.6lf ", matrix[i][j]);
            } else if ((j == m - 1) && (i != n - 1)) {
                printf("%.6lf\n", matrix[i][j]);
            } else if ((j == m - 1) && (i == n - 1)) {
                printf("%.6lf", matrix[i][j]);
            }
}

void invert(double **matrix, int n, int m) {
    int f = n;
    double **b = malloc((n) * (m) * sizeof(double) + (n) * sizeof(double *));
    double *ptr1 = (double *)(b + n);
    for (int i = 0; i < n; i++) b[i] = ptr1 + (m)*i;
    double **fac = malloc((n) * (m) * sizeof(double) + (n) * sizeof(double *));
    double *ptr2 = (double *)(fac + n);
    for (int i = 0; i < n; i++) fac[i] = ptr2 + (m)*i;
    int p, q, m1, n1, i, j;
    for (q = 0; q < f; q++) {
        for (p = 0; p < f; p++) {
            m1 = 0;
            n1 = 0;
            for (i = 0; i < f; i++) {
                for (j = 0; j < f; j++) {
                    if (i != q && j != p) {
                        b[m1][n1] = matrix[i][j];
                        if (n1 < (f - 2))
                            n1++;
                        else {
                            n1 = 0;
                            m1++;
                        }
                    }
                }
            }
            fac[q][p] = pow(-1, q + p) * det(b, f - 1);
        }
    }
    transpose(matrix, n, m, fac);
    free(fac);
    free(b);
}

int transpose(double **matrix, int n, int m, double **fac) {
    int n_result = m;
    int m_result = n;
    double **matrix_result = malloc((n_result) * (m_result) * sizeof(double) + (n_result) * sizeof(double *));
    double *ptr3 = (double *)(matrix_result + n_result);
    for (int i = 0; i < n_result; i++) matrix_result[i] = ptr3 + (m_result)*i;
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++) matrix_result[j][i] = fac[i][j];
    double d = det(matrix, n);
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            matrix_result[i][j] = matrix_result[i][j] / d;
        }
    }
    output(matrix_result, n, m);
    free(matrix_result);
    return 0;
}

void getMatrixWithoutRowAndCol(double **matrix, int size, int row, int col, double **newMatrix) {
    int offsetRow = 0;
    int offsetCol = 0;
    for (int i = 0; i < size - 1; i++) {
        if (i == row) {
            offsetRow = 1;
        }

        offsetCol = 0;
        for (int j = 0; j < size - 1; j++) {
            if (j == col) {
                offsetCol = 1;
            }

            newMatrix[i][j] = matrix[i + offsetRow][j + offsetCol];
        }
    }
}

double det(double **matrix, int n) {
    double sum = 0;
    double degree = 1;
    int size = n;

    if (size == 1) {
        sum = matrix[0][0];
    }

    else if (size == 2) {
        sum = matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];
    } else {
        double **newMatrix = malloc((size - 1) * (size - 1) * sizeof(double) + (size - 1) * sizeof(double *));
        double *ptr2 = (double *)(newMatrix + size - 1);
        for (int i = 0; i < size - 1; i++) newMatrix[i] = ptr2 + (size - 1) * i;

        for (int j = 0; j < size; j++) {
            getMatrixWithoutRowAndCol(matrix, size, 0, j, newMatrix);
            sum = sum + (degree * matrix[0][j] * det(newMatrix, size - 1));
            degree = -degree;
        }
        free(newMatrix);
    }

    return sum;
}