#include <stdio.h>
#include <stdlib.h>

void sort_vertical(int *matrix, int n, int m, int **result_matrix);
void sort_horizontal(int *matrix, int n, int m, int **result_matrix);

int input(int *matrix, int *n, int *m);
void output(int **a, int *stroki, int *stolb);
void swap(int *xp, int *yp);
void quick_sort(int *a, int first, int last);

int main() {
    int n1 = 0, m1 = 0;
    int *n = &n1, *m = &m1;
    int q = 1;
    char probel1, probel2;
    if ((scanf("%d%c%d%c", n, &probel1, m, &probel2) != 4) || ((*n <= 0) || (*m <= 0)) ||
        ((probel1 != '\n') && (probel1 != ' ') && (probel2 != ' ') && (probel2 != '\n'))) {
        printf("n/a");
        q = 0;
    }
    if (q) {
        int matrix[(*n) * (*m)];
        if (input(matrix, n, m)) {
            int **result_matrix = malloc((*n) * (*m) * sizeof(int) + (*n) * sizeof(int *));
            int *ptr3 = (int *)(result_matrix + *n);
            for (int i = 0; i < *n; i++) result_matrix[i] = ptr3 + (*m) * i;

            quick_sort(matrix, 0, (*n) * (*m) - 1);
            sort_vertical(matrix, *n, *m, result_matrix);
            output(result_matrix, n, m);
            printf("\n\n");
            sort_horizontal(matrix, *n, *m, result_matrix);
            output(result_matrix, n, m);
            free(result_matrix);
        }
    }
    return 1;
}

int input(int *matrix, int *n, int *m) {
    char probel;
    int q = 1;
    for (int i = 0; i < (*n) * (*m); i++)
        if ((scanf("%d%c", &(matrix[i]), &probel) != 2) || ((probel != '\n') && (probel != ' '))) {
            q = 0;
        }
    if ((!q) || (matrix == NULL)) {
        printf("n/a");
        q = 0;
    }

    return q;
}

void output(int **a, int *stroki, int *stolb) {
    for (int i = 0; i < *stroki; i++)
        for (int j = 0; j < *stolb; j++)
            if (j < *stolb - 1) {
                printf("%d ", a[i][j]);
            } else if ((j == *stolb - 1) && (i != *stroki - 1)) {
                printf("%d\n", a[i][j]);
            } else if ((j == *stolb - 1) && (i == *stroki - 1)) {
                printf("%d", a[i][j]);
            }
}
void sort_vertical(int *matrix, int n, int m, int **result_matrix) {
    int c = 0;
    for (int j = 0; j < m; j++)
        for (int i = 0; i < n; i++) {
            result_matrix[i][j] = matrix[c];
            c++;
        }

    for (int j = 0; j < m; j++)
        if (j % 2 != 0) {
            for (int i = 0; i < n; i++) {
                if (n > 1) {
                    if (i <= (n - 1) / 2) {
                        swap(&result_matrix[i][j], &result_matrix[n - i - 1][j]);
                    }
                } else {
                    if (i <= 1) {
                        swap(&result_matrix[i][j], &result_matrix[n - i - 1][j]);
                    }
                }
            }
        }
}

void sort_horizontal(int *matrix, int n, int m, int **result_matrix) {
    int c = 0;
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++) {
            result_matrix[i][j] = matrix[c];
            c++;
        }

    for (int i = 0; i < n; i++)
        if (i % 2 != 0) {
            for (int j = 0; j < m; j++) {
                if (m > 1) {
                    if (j <= (m - 1) / 2) {
                        swap(&result_matrix[i][j], &result_matrix[i][m - j - 1]);
                    }
                } else {
                    if (j <= 1) {
                        swap(&result_matrix[i][j], &result_matrix[i][m - j - 1]);
                    }
                }
            }
        }
}

void swap(int *xp, int *yp) {
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}

void quick_sort(int *a, int first, int last) {
    if (first < last) {
        int left = first, right = last, middle = a[(left + right) / 2];
        while (left <= right) {
            while (a[left] < middle) left++;
            while (a[right] > middle) right--;
            if (left <= right) {
                swap(a + left, a + right);
                left++;
                right--;
            }
        }
        quick_sort(a, first, right);
        quick_sort(a, left, last);
    }
}