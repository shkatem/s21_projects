#include "s21_grep.h"

int main(int argc, char *argv[]) {
  Flags flag = {0};
  Flags *flags = &flag;
  parcer(argc, argv, flags);
  while (optind < argc) {
    output(argv, flags);
    optind++;
  }
  return 1;
};

void parcer(int argc, char *argv[], Flags *flags) {
  int num_files = argc - optind;
  const char *short_options = "e:ivclnhsf:o";
  flags->num_files = 0;
  int rez;
  while ((rez = getopt_long(argc, argv, short_options, NULL, 0)) != -1) {
    switch (rez) {
      case 'e': {
        flags->e = 1;
        if (num_files > 1) flags->num_files = 1;
        strcat(flags->args, optarg);
        strcat(flags->args, "|");
        break;
      };
      case 'i': {
        flags->i = 1;
        break;
      };
      case 'v': {
        flags->v = 1;
        break;
      };
      case 'c': {
        flags->c = 1;
        break;
      };
      case 'l': {
        flags->l = 1;
        break;
      };
      case 'n': {
        flags->n = 1;
        break;
      };
      case 'h': {
        flags->h = 1;
        break;
      };
      case 's': {
        flags->s = 1;
        break;
      };
      case 'f': {
        flags->f = 1;
        break;
      };
      case 'o': {
        flags->o = 1;
        break;
      };
      case '?':
      default: {
        exit(EXIT_FAILURE);
      };
    };
  }
  if (flags->v && flags->o) flags->o = 0;
  if (flags->v && flags->c) flags->o = 0;
  if (argc - optind > 2) flags->num_files = 1;
  if (!flags->f && !flags->e) {
    if (argc > optind) {
      strcat(flags->args, argv[optind]);
    }
  } else {
    flags->args[strlen(flags->args) - 1] = '\0';
  }
};

void output(char *argv[], Flags *flags) {
  regex_t reg;
  regmatch_t start;
  int line_counter = 0;
  int matchlines = 0;
  int flag_i = REG_EXTENDED;
  FILE *fileptr;
  if (flags->i) {
    flag_i = REG_EXTENDED | REG_ICASE;
  }
  regcomp(&reg, flags->args, flag_i);
  fileptr = fopen(argv[optind], "r");
  if (fileptr != NULL) {
    strcpy(flags->filename, argv[optind]);

    while (fgets(flags->lines, BUFFER, fileptr) != NULL) {
      int match = regexec(&reg, flags->lines, 1, &start, 0);
      if ((!match || flags->v) && flags->num_files && !flags->l && !flags->h &&
          !flags->f) {
        printf("%s:", flags->filename);
      }
      if (flags->v) {
        match = !match;
      }
      if (!match) {
        if (flags->n) printf("%d:", line_counter + 1);
        if (!flags->c && !flags->l) {
          if (flags->e && flags->num_files > 1) printf("%s:", argv[optind]);
          printf("%s", flags->lines);  // это собственно сам принт совпадений
        }

        matchlines++;
      }
      line_counter++;
    }
    fclose(fileptr);
    if (flags->l) {
      printf("%s\n", flags->filename);  // flag l
    }
    if (flags->c) {
      if (!flags->h && flags->num_files > 1) printf("%s:", argv[optind]);
      if (flags->l && matchlines) {
        printf("1\n");
      } else {
        printf("%d\n", matchlines);
      }
    }  // flag c
  } else if (!flags->s && (optind > 1)) {
    fprintf(stderr, "s21_grep %s: No such file or directory\n", argv[optind]);
  }
  regfree(&reg);

  // regfree(&start);
}
