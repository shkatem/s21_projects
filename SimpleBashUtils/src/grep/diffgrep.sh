filename1=diffgrep.sh;
filename2=s21_grep.c;
filename3=s21_grep.h;
echo ----------------------------------------------*GREP 1
bash -c "diff <(grep int ${filename2} "$filename1") <(./s21_grep int "$filename2" "$filename1") -s -q"
echo ----------------------------------------------*GREP 1 -e
bash -c "diff <(grep -e int "$filename2" "$filename1") <(./s21_grep -e int "$filename2" "$filename1") -s -q"
echo ----------------------------------------------*GREP 2
bash -c "diff <(grep -i int "$filename1") <(./s21_grep -i int "$filename1") -s -q"
echo ----------------------------------------------*GREP 3
bash -c "diff <(grep -v int "$filename1") <(./s21_grep -v int "$filename1") -s -q"
echo ----------------------------------------------*GREP 4
bash -c "diff <(grep -c int "$filename1") <(./s21_grep -c int "$filename1") -s -q"
echo ----------------------------------------------*GREP 5
bash -c "diff <(grep -l int "$filename2" "$filename3" "$filename1") <(./s21_grep -l int "$filename2" "$filename3" "$filename1") -s -q"
echo ----------------------------------------------*GREP 6
bash -c "diff <(grep -n int "$filename1") <(./s21_grep -n int "$filename1") -s -q"
echo ----------------------------------------------*GREP 7 BONUS
bash -c "diff <(grep -h int "$filename1") <(./s21_grep -h int "$filename1") -s -q"
echo ----------------------------------------------*GREP 8 BONUS
bash -c "diff <(grep -o int "$filename1") <(./s21_grep -o int "$filename1") -s -q"
echo ----------------------------------------------*GREP 9 BONUS
bash -c "diff <(grep -h int "$filename2" "$filename1") <(./s21_grep -h int "$filename2" "$filename1") -s -q"
echo ----------------------------------------------*GREP 10 BONUS
bash -c "diff <(grep int -s aboba) <(./s21_grep int -s aboba) -s -q"
echo ----------------------------------------------*GREP 11 BONUS
bash -c "diff <(grep -f "$filename3" "$filename2") <(./s21_grep -f "$filename3" "$filename2") -s -q"
echo ----------------------------------------------*GREP 12 BONUS
bash -c "diff <(grep -in int "$filename1") <(./s21_grep -in int "$filename1") -s -q"
echo ----------------------------------------------*GREP 13 BONUS
bash -c "diff <(grep -cv int "$filename1") <(./s21_grep -cv int "$filename1") -s -q"
echo ----------------------------------------------*GREP 14 BONUS
bash -c "diff <(grep -iv int "$filename1") <(./s21_grep -iv int "$filename1") -s -q"
echo ----------------------------------------------*GREP 15 BONUS
bash -c "diff <(grep -lv int "$filename2" "$filename3" "$filename1") <(./s21_grep -lv int "$filename2" "$filename3" "$filename1") -s -q"
echo ----------------------------------------------*GREP 16 BONUS
bash -c "diff <(grep -ho int "$filename2" "$filename1") <(./s21_grep -ho int "$filename2" "$filename1") -s -q"
echo ----------------------------------------------*GREP 17 BONUS
bash -c "diff <(grep -nf "$filename3" "$filename2") <(./s21_grep -nf "$filename3" "$filename2") -s -q"
echo ----------------------------------------------*Tests ended*
