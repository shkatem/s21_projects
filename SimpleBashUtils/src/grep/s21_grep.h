#include <getopt.h>
#include <regex.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER 15000

typedef struct Flags {
  int e;
  int i;
  int v;
  int c;
  int l;
  int n;
  int h;
  int s;
  int f;
  int o;
  char args[BUFFER];      // массив для шаблонов
  char lines[BUFFER];     // массив для строк из файла
  char filename[BUFFER];  // массив для названия файла
  int num_files;
  char cropped_line_o[BUFFER];
} Flags;

void parcer(int argc, char *argv[], Flags *flags);
void output(char *argv[], Flags *flags);