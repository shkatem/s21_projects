filename=Makefile;
echo ----------------------------------------------*CAT 1
bash -c 'diff <(cat -b test_files/test_6_cat.txt) <(./s21_cat -b test_files/test_6_cat.txt) -s'
# diff <(cat -b test_files/test_6_cat.txt) <(./s21_cat -b test_files/test_6_cat.txt) -s
echo ----------------------------------------------*CAT 2
bash -c 'diff <(cat -e "test_files/test_2_cat.txt") <(./s21_cat -e "test_files/test_2_cat.txt") -s'
echo ----------------------------------------------*CAT 3
bash -c 'diff <(cat -n "test_files/test_3_cat.txt") <(./s21_cat -n "test_files/test_3_cat.txt") -s'
echo ----------------------------------------------*CAT 4
bash -c 'diff <(cat -n "test_files/test_4_cat.txt") <(./s21_cat --number "test_files/test_4_cat.txt") -s'
echo ----------------------------------------------*CAT 5
bash -c 'diff <(cat -s "test_files/test_5_cat.txt") <(./s21_cat -s "test_files/test_5_cat.txt") -s'
echo ----------------------------------------------*CAT 6
bash -c 'diff <(cat -t "test_files/test_6_cat.txt") <(./s21_cat -t "test_files/test_6_cat.txt") -s'

