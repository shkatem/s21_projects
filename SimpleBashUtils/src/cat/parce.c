#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>

#include "functions.h"

int main(int argc, char *argv[]) {
  Flags *flags, fl;
  static Flags EmptyStruct;
  fl = EmptyStruct;
  flags = &fl;
  const char *short_options = "benstvET";

  const struct option long_options[] = {
      {"number-nonblank", no_argument, NULL, 'b'},
      {"number", no_argument, NULL, 'n'},
      {"squeeze-blank", no_argument, NULL, 's'},
      {NULL, 0, NULL, 0}};

  int rez;
  int option_index = -1;

  while ((rez = getopt_long(argc, argv, short_options, long_options,
                            &option_index)) != -1) {
    switch (rez) {
      case 'b': {
        flags->_bflag = 1;
        break;
      };
      case 'e': {
        flags->_eflag = 1;
        flags->_vflag = 1;
        break;
      };
      case 'E': {
        flags->_eflag = 1;
        break;
      };
      case 'n': {
        flags->_nflag = 1;
        break;
      };
      case 's': {
        flags->_sflag = 1;
        break;
      };
      case 't': {
        flags->_tflag = 1;
        flags->_vflag = 1;
        break;
      };
      case 'T': {
        flags->_tflag = 1;
        break;
      };
      case 'v': {
        flags->_vflag = 1;
        break;
      };
      case '?':
      default: {
        printf("found unknown option\n");
        break;
      };
    };
    option_index = -1;
  };
  // When getopt can't find any more short options, it returns -1 and sets the
  // global variable optind to the next element in **argv after all the short
  // options. printf("b %d e %d n %d s %d t %d v  %d ", flags->_bflag,
  // flags->_eflag, flags->_nflag, flags->_sflag , flags->_tflag,
  // flags->_vflag);
  functions(argc, argv, flags);
  // free(flags);
  return 0;
};