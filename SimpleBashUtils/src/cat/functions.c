
#include "functions.h"

int functions(int argc, char **argv, Flags *flags) {
  int fut_ch;
  int last_ch = '\n';
  int strings_counter = 0;
  int empty_str_counter = 0;
  FILE *fileptr;

  for (int i = 1; i < argc; i++) {
    fileptr = fopen(argv[i], "r");
    if (fileptr != NULL) {
      while ((fut_ch = fgetc(fileptr)) != EOF) {
        if (flags->_sflag) {
          if ((empty_str_counter = s(fut_ch, last_ch, empty_str_counter)) > 1)
            continue;
        }
        if (flags->_bflag) {
          strings_counter = b(fut_ch, last_ch, strings_counter);
        }
        if (flags->_nflag) {
          strings_counter = n(last_ch, strings_counter);
        }
        if (flags->_tflag) {
          fut_ch = t(fut_ch);
        }
        if (flags->_eflag) {
          e(fut_ch);
        }
        if (flags->_vflag) {
          fut_ch = v(fut_ch);
        }

        printf("%c", fut_ch);
        last_ch = fut_ch;
      }
      fclose(fileptr);
    }
  }

  return 0;
}

int s(char fut_ch, char last_ch, int empty_str_counter) {
  if (fut_ch == '\n' && last_ch == '\n') {
    empty_str_counter++;
  } else {
    empty_str_counter = 0;
  }
  return empty_str_counter;
}

int b(char fut_ch, char last_ch, int strings_counter) {
  if (fut_ch != '\n' && last_ch == '\n') {
    printf("%6d\t", ++strings_counter);
  }
  return strings_counter;
}

int n(char last_ch, int strings_counter) {
  if (last_ch == '\n') {
    printf("%6d\t", ++strings_counter);
  }
  return strings_counter;
}
int t(char fut_ch) {
  if (fut_ch == '\t') {
    printf("^");
    fut_ch = 'I';
  }
  return fut_ch;
}
void e(int fut_ch) {
  if (fut_ch == '\n') {
    printf("$");
  }
}
int v(int fut_ch) {
  if ((fut_ch >= 0 && fut_ch < 9) || (fut_ch > 10 && fut_ch < 32) ||
      (fut_ch > 126 && fut_ch < 160)) {
    printf("^");
    if (fut_ch > 126) {
      fut_ch -= 64;
    } else {
      fut_ch += 64;
    }
  }
  return fut_ch;
}