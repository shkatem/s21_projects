#include <stdio.h>

#include "parce.h"
int functions(int argc, char **argv, Flags *flags);

int s(char fut_ch, char last_ch, int empty_str_counter);
int b(char fut_ch, char last_ch, int strings_counter);
int n(char last_ch, int strings_counter);
int t(char fut_ch);
void e(int fut_ch);
int v(int fut_ch);