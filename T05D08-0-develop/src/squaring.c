#include <stdio.h>
#define NMAX 10

int input(int *a, int *pn);
void output(int *a, const int *pn);
void squaring(int *a, const int *pn);

int main() {
    int n = 0, q = 1;
    int *pn = &n;
    int data[NMAX];
    if (!input(data, pn)) {
        q = 0;
    }
    if (q != 0) {
        squaring(data, pn);
        output(data, pn);
    }

    return q;
}

int input(int *a, int *pn) {
    char probel;
    int q = 1;
    //    million checks for a right format
    if ((scanf("%d%c", pn, &probel) != 2) || (probel != '\n') || (*pn > NMAX) || (*pn <= 0)) {
        printf("n/a");
        q = 0;
    }
    if (q != 0) {
        for (int i = 0; i <= *pn; i++) {
            if (i < (*pn - 1)) {
                if ((scanf("%d%c", &a[i], &probel) != 2) || ((probel != '\n') && (probel != ' '))) {
                    printf("n/a");
                    q = 0;
                    break;
                }
            } else if (i == (*pn - 1)) {
                if ((scanf("%d%c", &a[i], &probel) != 2) || ((probel != '\n'))) {
                    printf("n/a");
                    q = 0;
                    break;
                }
            }
        }
    }
    return q;
}

void output(int *a, const int *pn) {
    for (int i = 0; i < *pn; i++) {
        if (i != *pn - 1) printf("%d ", a[i]);
        if (i == *pn - 1) printf("%d", a[i]);
    }
}

void squaring(int *a, const int *pn) {
    for (int i = 0; i < *pn; i++) {
        a[i] *= a[i];
    }
}
