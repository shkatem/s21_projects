#include <stdio.h>

void maxmin(float* pmax, float* pmin, float prob1, float prob2, float prob3);

/* Find a max & min probabilities */
int main() {
    float x, y, z;
    char probel;
    if (scanf("%f %f %f%c", &x, &y, &z, &probel) != 4) {
        printf("n/a");
        return 0;
    }
    if ((x != (int)x) || (y != (int)y) || (z != (int)z) || (probel != '\n')) {
        printf("n/a");
        return 0;
    }
    float max, min;
    float* pmax = &max;
    float* pmin = &min;

    maxmin(pmax, pmin, x, y, z);

    printf("%.0f %.0f", max, min);

    return 0;
}

/* This function should be kept !!! (Your AI) */
/* But errors & bugs should be fixed         */
void maxmin(float* pmax, float* pmin, float prob1, float prob2, float prob3) {
    *pmax = *pmin = prob1;

    if (prob2 > *pmax) *pmax = prob2;
    if (prob2 < *pmin) *pmin = prob2;
    if (prob3 > *pmax) *pmax = prob3;
    if (prob3 < *pmin) *pmin = prob3;
}
