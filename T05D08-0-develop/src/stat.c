#include <stdio.h>
#define NMAX 10

int input(int *a, int *pn);
// void output(int *a, int *pn);
int max(const int *a, const int *pn);
int min(const int *a, const int *pn);
double mean(const int *a, const int *pn);
double variance(const int *a, const int *pn);
//
void output_result(int max_v, int min_v, double mean_v, double variance_v);

int main() {
    int n = 0, q = 1;
    int *pn = &n;
    int data[NMAX];
    if (!input(data, pn)) {
        q = 0;
    }
    if (q != 0) {
        output_result(max(data, pn), min(data, pn), mean(data, pn), variance(data, pn));
    }

    return 0;
}
int input(int *a, int *pn) {
    char probel;
    int q = 1;
    //    million checks for a right format
    if ((scanf("%d%c", pn, &probel) != 2) || (probel != '\n') || (*pn > NMAX) || (*pn <= 0)) {
        printf("n/a");
        q = 0;
    }
    //    million checks for a right format
    if (q != 0) {
        for (int i = 0; i <= *pn; i++) {
            if (i < (*pn - 1)) {
                if ((scanf("%d%c", &a[i], &probel) != 2) || ((probel != '\n') && (probel != ' '))) {
                    printf("n/a");
                    q = 0;
                    break;
                }
            } else if (i == (*pn - 1)) {
                if ((scanf("%d%c", &a[i], &probel) != 2) || ((probel != '\n'))) {
                    printf("n/a");
                    q = 0;
                    break;
                }
            }
        }
    }
    return q;
}

int max(const int *a, const int *pn) {
    int max_v = a[0];
    for (int i = 1; i < *pn; ++i) {
        if (a[i] > max_v) {
            max_v = a[i];
        }
    }
    return max_v;
}
int min(const int *a, const int *pn) {
    int min_v = a[0];
    for (int i = 1; i < *pn; ++i) {
        if (a[i] < min_v) {
            min_v = a[i];
        }
    }
    return min_v;
}
double mean(const int *a, const int *pn) {
    double mean_v = 0;
    for (int i = 0; i < *pn; ++i) {
        mean_v += a[i] / ((double)*pn);
    }
    return mean_v;
}
double variance(const int *a, const int *pn) {
    double mean_v = 0;
    double mean_v_squar = 0;
    for (int i = 0; i < *pn; ++i) {
        mean_v_squar += (a[i] * a[i]) / ((double)*pn);
        mean_v += a[i] / ((double)*pn);
    }
    return mean_v_squar - mean_v * mean_v;
}

void output_result(int max_v, int min_v, double mean_v, double variance_v) {
    printf("%d %d %.6f %.6f", max_v, min_v, mean_v, variance_v);
}
