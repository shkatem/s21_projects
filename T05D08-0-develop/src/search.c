/*
    Search module for the desired value from data array.

    Returned value must be:
        - "even"
        - ">= mean"
        - "<= mean + 3 * sqrt(variance)"
        - "!= 0"

        OR

        0
*/
#include <math.h>
#include <stdio.h>
#define NMAX 10

int input(int *a, int *pn);
int max(int *a, int *pn);
int min(int *a, int *pn);
double mean(const int *a, int *pn);
double variance(const int *a, int *pn);
int check_all_numbers(const int *a, const int *pn, double mean_v, double variance_v);

int main() {
    int n = 0, q = 1;
    int *pn = &n;
    int data[NMAX];
    if (!input(data, pn)) {
        q = 0;
    }
    if (q != 0) {
        printf("%d", check_all_numbers(data, pn, mean(data, pn), variance(data, pn)));
    }

    return 0;
}
int input(int *a, int *pn) {
    char probel;
    int q = 1;
    //    million checks for a right format
    if ((scanf("%d%c", pn, &probel) != 2) || (probel != '\n') || (*pn > NMAX) || (*pn <= 0)) {
        printf("n/a");
        q = 0;
    }
    //    million checks for a right format
    if (q != 0) {
        for (int i = 0; i <= *pn; i++) {
            if (i < (*pn - 1)) {
                if ((scanf("%d%c", &a[i], &probel) != 2) || ((probel != '\n') && (probel != ' '))) {
                    printf("n/a");
                    q = 0;
                    break;
                }
            } else if (i == (*pn - 1)) {
                if ((scanf("%d%c", &a[i], &probel) != 2) || ((probel != '\n'))) {
                    printf("n/a");
                    q = 0;
                    break;
                }
            }
        }
    }
    return q;
}

double mean(const int *a, int *pn) {
    double mean_v = 0;
    for (int i = 0; i < *pn; ++i) {
        mean_v += a[i] / ((double)*pn);
    }
    return mean_v;
}
double variance(const int *a, int *pn) {
    double mean_v = 0;
    double mean_v_squar = 0;
    for (int i = 0; i < *pn; ++i) {
        mean_v_squar += (a[i] * a[i]) / ((double)*pn);
        mean_v += a[i] / ((double)*pn);
    }
    return mean_v_squar - mean_v * mean_v;
}

int check_all_numbers(const int *a, const int *pn, double mean_v, double variance_v) {
    int result = 0;
    for (int i = 0; i < *pn; ++i) {
        if ((a[i] % 2 == 0) && (a[i] != 0) && (a[i] >= mean_v) &&
            (a[i] <= (mean_v + 3 * pow(variance_v, 0.5)))) {
            result = a[i];
            break;
        }
    }
    return result;
}
