
#include <math.h>
#include <stdio.h>

float delitel(float a);

int main() {
    float a;
    if (scanf("%f", &a) != 1) {
        printf("n/a");
        return 0;
    }
    if (a != (int)a) {
        printf("n/a");
        return 0;
    }
    if (a < 0) {
        a = -a;
    }
    if (a == 0) {
        printf("n/a");
        return 0;
    }
    int result = delitel(a);
    printf("%d", result);
    return 0;
}

float delitel(float a) {
    int i;
    int the_delitel = 1;
    for (i = a - 1; i > 1; --i) {
        int z = i;
        int diff = a;
        while (diff >= 0) {
            diff -= z;
            if (diff == 0) {
                the_delitel = z;
                a = the_delitel;
            }
        }
        diff = a;
        if (the_delitel == 1) {
            the_delitel = a;
        }
    }
    return the_delitel;
}
