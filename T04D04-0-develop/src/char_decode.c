
#include <math.h>
#include <stdio.h>

char code();
char decode();

int main(int argc, char **argv) {
    char job_description = *argv[1];
    if ((job_description != '0') && (job_description != '1')) {
        printf("n/a");
        return argc;
    }

    ////    1 part: from text to ascII
    //    int i =0;
    //    while( (a != '\n') &&(b != '\n') ){
    //        i+=2;
    //        if (scanf("%c%c", &a,&b) != i) {
    //            printf("n/a");
    //            return 0;
    //        }
    //        printf("%d%c", a,b);
    //    }

    ////    part 2: from ASCII to text:
    //    while ( (c != '\n') &&(d != '\n') )  {
    //    scanf("%X%c", &c,&d);
    //    printf("%c%c", c,d);
    //    }
    //    scanf("%c%c%c", &a,&b,&c);
    //    printf("%d%d%d", a,b,c);
    //    int back = (int)a*(int)b;
    //    printf("%c%c", back,c);
    //
    if (job_description == '0') {
        return code();
    }
    if (job_description == '1') {
        return decode();
    }
    return 0;
}

char code() {
    char a, b;
    while ((a != '\n') && (b != '\n')) {
        scanf("%c%c", &a, &b);
        if (((int)b != 32) && ((int)b != '\n')) {
            printf("n/a");
            return 0;
        }
        printf("%X%c", a, b);
    }
    return 0;
}

char decode() {
    int c;
    char d;
    while ((c != '\n') && (d != '\n')) {
        scanf("%X%c", &c, &d);
        if (((int)d != 32) && ((int)d != '\n')) {
            printf("n/a");
            return 0;
        }
        if ((int)c > 255) {
            printf("n/a");
            return 0;
        }
        printf("%c%c", c, d);
    }
    return 0;
}
