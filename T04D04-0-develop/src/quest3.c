
#include <math.h>
#include <stdio.h>

int fibo(float n);

int main() {
    float n;
    if (scanf("%f", &n) != 1) {
        printf("n/a");
        return 0;
    }
    if (n < 0) {
        printf("n/a");
        return 0;
    }
    if (n != (int)n) {
        printf("n/a");
        return 0;
    }
    printf("%d", fibo(n));
    return 0;
}

int fibo(float n) {
    if (n == 0) {
        return 0;
    }
    if (n == 1 || n == 2) {
        return 1;
    }
    return fibo(n - 1) + fibo(n - 2);
}
//
