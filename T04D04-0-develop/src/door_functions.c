
#include <math.h>
#include <stdio.h>

double va(double x);
double lb(double x);
double g(double x);

float pi = 3.14159265358979323846;

int main() {
    double x = -pi;
    int i;
    for (i = 0; i < 42; x += (2 * pi / 41)) {
        i++;
        if (x == 0) {
            printf("%.7f | %.7f | %.7f | -\n", x, va(x), lb(x));
        } else if ((x > pow(2.0, 0.5)) || (x < -pow(2.0, 0.5))) {
            printf("%.7f | %.7f | - | %.7f\n", x, va(x), g(x));
        } else {
            printf("%.7f | %.7f | %.7f | %.7f\n", x, va(x), g(x), lb(x));
        }
    }
    return 0;
}

double va(double x) {
    double y = pow(1.0, 3) / (pow(1.0, 2) + pow(x, 2));
    return y;
}
double g(double x) {
    double y = 1 / pow(x, 2);
    return y;
}
double lb(double x) {
    double m = pow(1.0, 4) + 4 * pow(x, 2) * pow(1.0, 2);
    double b = pow(m, 0.5) - pow(x, 2) - pow(1.0, 2);
    double y = pow(b, 0.5);
    return y;
}
