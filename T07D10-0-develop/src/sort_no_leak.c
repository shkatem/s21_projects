
#include <stdio.h>
#include <stdlib.h>

int input_num(int *n);
int input_data(int *a, int *n);
void quick_sort(int *a, int first, int last);
void swap(int *xp, int *yp);
void printArray(int *a, int *n);
void *malloc(size_t size);
void *realloc(void *ptr, size_t size);

int main() {
    int n = 0;
    int *number = &n;
    int *data = 0;
    data = (int *)malloc(input_num(number) * sizeof(int));
    if (input_data(data, number)) {
        quick_sort(data, 0, *number - 1);
        printArray(data, number);
    }
    free(data);
    return 0;
}

int input_num(int *n) {
    char probel;
    int q = 1;
    //    million checks for a right format
    if ((scanf("%d%c", n, &probel) != 2) || ((probel != '\n') && (probel != ' '))) {
        printf("n/a");
        q = 0;
    }
    return (*n) * q;
}
int input_data(int *a, int *n) {
    int q = 1;
    char probel;
    for (int i = 0; i <= *n; i++) {
        if (i < (*n - 1)) {
            if ((scanf("%d%c", &a[i], &probel) != 2) || ((probel != '\n') && (probel != ' '))) {
                printf("n/a");
                q = 0;
                break;
            }
        } else if (i == (*n - 1)) {
            if ((scanf("%d%c", &a[i], &probel) != 2) || ((probel != '\n'))) {
                printf("n/a");
                q = 0;
                break;
            }
        }
    }
    if ((q) && (a == NULL)) {
        printf("n/a");
        q = 0;
    }
    return q;
}

void quick_sort(int *a, int first, int last) {
    if (first < last) {
        int left = first, right = last, middle = a[(left + right) / 2];
        while (left <= right) {
            while (a[left] < middle) left++;
            while (a[right] > middle) right--;
            if (left <= right) {
                swap(a + left, a + right);
                left++;
                right--;
            }
        }
        quick_sort(a, first, right);
        quick_sort(a, left, last);
    }
}
void swap(int *xp, int *yp) {
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}

void printArray(int *a, int *n) {
    for (int i = 0; i < *n; ++i) {
        if (i != *n - 1) printf("%d ", a[i]);
        if (i == *n - 1) printf("%d", a[i]);
    }
}
