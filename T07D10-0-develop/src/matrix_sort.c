#include <stdio.h>
#include <stdlib.h>

int input_memory(int *m);
int input_nums(int *stroki, int *stolb);
int input_data(int **a, int *stroki, int *stolb);
void sort(int **a, int *stroki, int *stolb);
void quick_sort(int *a, int *b, int first, int last);
void swap(int *xp, int *yp);
void printArray(int **a, int *b, int *stroki, int *stolb);

int main() {
    int n = 0, k = 0, m = 1;
    int *stroki = &n, *stolb = &k, *memory = &m;
    int key = input_memory(memory);
    if (key != 0) {
        if (input_nums(stroki, stolb)) {
            switch (key) {
                case 1: {
                    int **single_array_matrix =
                        malloc((*stroki) * (*stolb) * sizeof(int) + (*stroki) * sizeof(int *));
                    int *ptr = (int *)(single_array_matrix + *stroki);
                    for (int i = 0; i < *stroki; i++) single_array_matrix[i] = ptr + (*stolb) * i;
                    if (input_data(single_array_matrix, stroki, stolb)) {
                        sort(single_array_matrix, stroki, stolb);
                    }
                    free(single_array_matrix);
                    break;
                }

                case 2: {
                    int **pointer_array3 = malloc((*stroki) * (sizeof(int *)));
                    for (int i = 0; i < *stroki; i++) pointer_array3[i] = malloc((*stolb) * (sizeof(int)));
                    if (input_data(pointer_array3, stroki, stolb)) {
                        sort(pointer_array3, stroki, stolb);
                    }
                    for (int i = 0; i < *stroki; i++) free(pointer_array3[i]);
                    free(pointer_array3);
                    break;
                }

                case 3: {
                    int **pointer_array4 = malloc((*stroki) * (sizeof(int *)));
                    int *values_array = malloc((*stroki) * (*stolb) * sizeof(int));
                    for (int i = 0; i < *stroki; i++) pointer_array4[i] = values_array + (*stolb) * i;
                    if (input_data(pointer_array4, stroki, stolb)) {
                        sort(pointer_array4, stroki, stolb);
                    }
                    free(values_array);
                    free(pointer_array4);
                    break;
                }
                default:
                    printf("n/a");
            }
        }
    }
    return 0;
}
int input_memory(int *m) {
    char probel;
    int q = 1;
    if ((scanf("%d%c", m, &probel) != 2) || ((probel != '\n') && (probel != ' ')) || (*m > 4) || (*m <= 0)) {
        printf("n/a");
        q = 0;
    }
    return (*m) * q;
}

int input_nums(int *stroki, int *stolb) {
    char probel1, probel2;
    int q = 1;
    if ((scanf("%d%c%d%c", stroki, &probel1, stolb, &probel2) != 4) || ((*stolb <= 0) || (*stroki <= 0)) ||
        ((probel1 != '\n') && (probel1 != ' ') && (probel2 != ' ') && (probel2 != '\n'))) {
        printf("n/a");
        q = 0;
    }
    return q;
}

int input_data(int **a, int *stroki, int *stolb) {
    int q = 1;
    char probel;
    for (int i = 0; i < *stroki; i++)
        for (int j = 0; j < *stolb; j++)
            if ((scanf("%d%c", &(a[i][j]), &probel) != 2) || ((probel != '\n') && (probel != ' '))) {
                q = 0;
            }
    if ((!q) || (a == NULL)) {
        printf("n/a");
        q = 0;
    }
    return q;
}

void sort(int **a, int *stroki, int *stolb) {
    int *new_array, *new_array_nums;
    new_array = (int *)calloc(*stroki, sizeof(int));
    new_array_nums = (int *)calloc(*stroki, sizeof(int));
    for (int i = 0; i < *stroki; i++) {
        for (int j = 0; j < *stolb; j++) {
            new_array[i] += a[i][j];
            new_array_nums[i] = i;
        }
    }
    quick_sort(new_array, new_array_nums, 0, *stroki - 1);
    printArray(a, new_array_nums, stroki, stolb);
    free(new_array), free(new_array_nums);
}

void quick_sort(int *a, int *b, int first, int last) {
    if (first < last) {
        int left = first, right = last, middle = a[(left + right) / 2];
        while (left <= right) {
            while (a[left] < middle) left++;
            while (a[right] > middle) right--;
            if (left <= right) {
                swap(a + left, a + right);
                swap(b + left, b + right);
                left++;
                right--;
            }
        }
        quick_sort(a, b, first, right);
        quick_sort(a, b, left, last);
    }
}
void swap(int *xp, int *yp) {
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}
void printArray(int **a, int *b, int *stroki, int *stolb) {
    for (int i = 0; i < *stroki; ++i) {
        for (int j = 0; j < *stolb; ++j) {
            if (j < *stolb - 1) {
                printf("%d ", a[b[i]][j]);
            } else if ((j == *stolb - 1) && (i != *stroki - 1)) {
                printf("%d\n", a[b[i]][j]);
            } else if ((j == *stolb - 1) && (i == *stroki - 1)) {
                printf("%d", a[b[i]][j]);
            }
        }
    }
}
