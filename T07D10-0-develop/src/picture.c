#include <stdio.h>

#define N 15
#define M 13

void transform(int **matr, int (*buf)[13], int n, int m);
void make_picture(int **picture, int n, int m);
void reset_picture(int **picture, int n, int m);
int print_static(int **a, int stroki, int stolb);

int main() {
    int picture_data[N][M];
    int *picture[N];
    transform(picture, picture_data, N, M);

    make_picture(picture, N, M);
    return 0;
}

void make_picture(int **picture, int n, int m) {
    int frame_w[] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
    int frame_h[] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
    int tree_trunk[] = {7, 7, 7, 7};
    int tree_foliage[] = {3, 3, 3, 3};
    int sun_data[6][5] = {{0, 6, 6, 6, 6}, {0, 0, 6, 6, 6}, {0, 0, 6, 6, 6},
                          {0, 6, 0, 0, 6}, {0, 0, 0, 0, 0}, {0, 0, 0, 0, 0}};

    reset_picture(picture, n, m);

    int length_frame_w = sizeof(frame_w) / sizeof(frame_w[0]);
    int length_frame_h = sizeof(frame_h) / sizeof(frame_h[0]);
    int length_frame_trunk = sizeof(tree_trunk) / sizeof(tree_trunk[0]);
    int length_tree_foliage = sizeof(tree_foliage) / sizeof(tree_foliage[0]);

    for (int j = 0; j < length_frame_trunk; j++) {
        picture[j + 3][3] = tree_trunk[j];
        picture[j + 3][4] = tree_trunk[j];
        picture[j + 7][3] = tree_trunk[j];
        picture[j + 7][4] = tree_trunk[j];
        picture[10][j + 2] = tree_trunk[j];
    }

    for (int j = 0; j < length_tree_foliage; j++) {
        picture[j + 2][3] = tree_foliage[j];
        picture[j + 2][4] = tree_foliage[j];
        picture[3][j + 2] = tree_foliage[j];
        picture[4][j + 2] = tree_foliage[j];
    }

    for (int i = 0; i < 6; i++)
        for (int j = 0; j < 5; j++) picture[i + 1][j + 7] = sun_data[i][j];

    for (int i = 0; i < length_frame_w; i++) {
        picture[0][i] = frame_w[i];
        picture[7][i] = frame_w[i];
        picture[14][i] = frame_w[i];
    }
    for (int j = 0; j < length_frame_h; j++) {
        picture[j][0] = frame_h[j];
        picture[j][6] = frame_h[j];
        picture[j][M - 1] = frame_h[j];
    }

    print_static(picture, N, M);
}

void reset_picture(int **picture, int n, int m) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            picture[i][j] = 0;
        }
    }
}

void transform(int **matr, int (*buf)[13], int n, int m) {
    for (int i = 0; i < n; i++) {
        matr[i] = buf[0] + i * m;
    }
}

int print_static(int **a, int stroki, int stolb) {
    for (int i = 0; i < stroki; i++)
        for (int j = 0; j < stolb; j++)
            if (j < stolb - 1) {
                printf("%d ", a[i][j]);
            } else if ((j == stolb - 1) && (i != stroki - 1)) {
                printf("%d\n", a[i][j]);
            } else if ((j == stolb - 1) && (i == stroki - 1)) {
                printf("%d", a[i][j]);
            }
    return 0;
}
