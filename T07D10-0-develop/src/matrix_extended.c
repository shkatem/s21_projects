
#include <stdio.h>
#include <stdlib.h>
#define NMAX 100

int input_memory(int *m);
int input_nums(int *stroki, int *stolb);
int input_data(int **a, int *stroki, int *stolb);
int input_static(int (*a)[NMAX], int *stroki, int *stolb);
int print_static(int (*a)[NMAX], int *stroki, int *stolb);
void search_for_max_stroka(int **a, int *stroki, int *stolb);
void search_for_min_stolb(int **a, int *stroki, int *stolb);
void search_for_max_stroka_static(int (*a)[NMAX], int *stroki, int *stolb);
void search_for_min_stolb_static(int (*a)[NMAX], int *stroki, int *stolb);
void *malloc(size_t size);
void print_Array(int **a, int *stroki, int *stolb);

int main() {
    int n = 0, k = 0, m = 1;
    int *stroki = &n, *stolb = &k, *memory = &m;
    int key = input_memory(memory);
    if (key != 0) {
        if (input_nums(stroki, stolb)) {
            switch (key) {
                case 1: {
                    int data[NMAX][NMAX];
                    if (input_static(data, stroki, stolb)) {
                        print_static(data, stroki, stolb);
                        search_for_max_stroka_static(data, stroki, stolb);
                        search_for_min_stolb_static(data, stroki, stolb);
                    }
                    break;
                }
                case 2: {
                    int **single_array_matrix =
                        malloc((*stroki) * (*stolb) * sizeof(int) + (*stroki) * sizeof(int *));
                    int *ptr = (int *)(single_array_matrix + *stroki);
                    for (int i = 0; i < *stroki; i++) single_array_matrix[i] = ptr + (*stolb) * i;
                    if (input_data(single_array_matrix, stroki, stolb)) {
                        print_Array(single_array_matrix, stroki, stolb);
                        search_for_max_stroka(single_array_matrix, stroki, stolb);
                        search_for_min_stolb(single_array_matrix, stroki, stolb);
                    }
                    free(single_array_matrix);
                    break;
                }

                case 3: {
                    int **pointer_array3 = malloc((*stroki) * (sizeof(int *)));
                    for (int i = 0; i < *stroki; i++) pointer_array3[i] = malloc((*stolb) * (sizeof(int)));
                    if (input_data(pointer_array3, stroki, stolb)) {
                        print_Array(pointer_array3, stroki, stolb);
                        search_for_max_stroka(pointer_array3, stroki, stolb);
                        search_for_min_stolb(pointer_array3, stroki, stolb);
                    }
                    for (int i = 0; i < *stroki; i++) free(pointer_array3[i]);
                    free(pointer_array3);
                    break;
                }

                case 4: {
                    int **pointer_array4 = malloc((*stroki) * (sizeof(int *)));
                    int *values_array = malloc((*stroki) * (*stolb) * sizeof(int));
                    for (int i = 0; i < *stroki; i++) pointer_array4[i] = values_array + (*stolb) * i;
                    if (input_data(pointer_array4, stroki, stolb)) {
                        print_Array(pointer_array4, stroki, stolb);
                        search_for_max_stroka(pointer_array4, stroki, stolb);
                        search_for_min_stolb(pointer_array4, stroki, stolb);
                    }
                    free(values_array);
                    free(pointer_array4);
                    break;
                }
                default:
                    printf("n/a");
            }
        }
    }
    return 0;
}
int input_memory(int *m) {
    char probel;
    int q = 1;
    if ((scanf("%d%c", m, &probel) != 2) || ((probel != '\n') && (probel != ' ')) || (*m > 4) || (*m <= 0)) {
        printf("n/a");
        q = 0;
    }
    return (*m) * q;
}

int input_nums(int *stroki, int *stolb) {
    char probel1, probel2;
    int q = 1;
    if ((scanf("%d%c%d%c", stroki, &probel1, stolb, &probel2) != 4) || ((*stolb <= 0) || (*stroki <= 0)) ||
        ((probel1 != '\n') && (probel1 != ' ') && (probel2 != ' ') && (probel2 != '\n'))) {
        printf("n/a");
        q = 0;
    }
    return q;
}

int input_static(int (*a)[NMAX], int *stroki, int *stolb) {
    int q = 1;
    char probel;
    for (int i = 0; i < *stroki; i++)
        for (int j = 0; j < *stolb; j++)
            if ((scanf("%d%c", &(a[i][j]), &probel) != 2) || ((probel != '\n') && (probel != ' '))) {
                q = 0;
            }
    if ((!q) || (a == NULL)) {
        printf("n/a");
        q = 0;
    }
    return q;
}

int print_static(int (*a)[NMAX], int *stroki, int *stolb) {
    for (int i = 0; i < *stroki; i++)
        for (int j = 0; j < *stolb; j++)
            if (j < *stolb - 1) {
                printf("%d ", a[i][j]);
            } else if ((j == *stolb - 1) && (i != *stroki - 1)) {
                printf("%d\n", a[i][j]);
            } else if ((j == *stolb - 1) && (i == *stroki - 1)) {
                printf("%d\n", a[i][j]);
            }
    return 1;
}

int input_data(int **a, int *stroki, int *stolb) {
    int q = 1;
    char probel;
    for (int i = 0; i < *stroki; i++)
        for (int j = 0; j < *stolb; j++)
            if ((scanf("%d%c", &(a[i][j]), &probel) != 2) || ((probel != '\n') && (probel != ' '))) {
                q = 0;
            }
    if ((!q) || (a == NULL)) {
        printf("n/a");
        q = 0;
    }
    return q;
}

void print_Array(int **a, int *stroki, int *stolb) {
    for (int i = 0; i < *stroki; i++)
        for (int j = 0; j < *stolb; j++)
            if (j < *stolb - 1) {
                printf("%d ", a[i][j]);
            } else if ((j == *stolb - 1) && (i != *stroki - 1)) {
                printf("%d\n", a[i][j]);
            } else if ((j == *stolb - 1) && (i == *stroki - 1)) {
                printf("%d\n", a[i][j]);
            }
}

void search_for_max_stroka(int **a, int *stroki, int *stolb) {
    int new_array[*stroki];
    for (int i = 0; i < *stroki; i++) {
        int max_v = a[i][0];
        for (int j = 0; j < *stolb; j++) {
            if (a[i][j] > max_v) {
                max_v = a[i][j];
            }
        }
        new_array[i] = max_v;
        max_v = a[i][0];
    }
    for (int i = 0; i < *stroki; i++) {
        if (i < *stroki - 1) {
            printf("%d ", new_array[i]);
        } else if (i == *stolb - 1) {
            printf("%d\n", new_array[i]);
        }
    }
}

void search_for_min_stolb(int **a, int *stroki, int *stolb) {
    int new_array[*stolb];
    for (int i = 0; i < *stolb; i++) {
        int min_v = a[0][i];
        for (int j = 0; j < *stroki; j++) {
            if (a[j][i] < min_v) {
                min_v = a[j][i];
            }
        }
        new_array[i] = min_v;
        min_v = a[0][i];
    }
    for (int i = 0; i < *stolb; i++) {
        if (i < *stroki - 1) {
            printf("%d ", new_array[i]);
        } else if (i == *stolb - 1) {
            printf("%d", new_array[i]);
        }
    }
}

void search_for_max_stroka_static(int (*a)[NMAX], int *stroki, int *stolb) {
    int new_array[*stroki];
    for (int i = 0; i < *stroki; i++) {
        int max_v = a[i][0];
        for (int j = 0; j < *stolb; j++) {
            if (a[i][j] > max_v) {
                max_v = a[i][j];
            }
        }
        new_array[i] = max_v;
        max_v = a[i][0];
    }
    for (int i = 0; i < *stroki; i++) {
        if (i < *stroki - 1) {
            printf("%d ", new_array[i]);
        } else if (i == *stolb - 1) {
            printf("%d\n", new_array[i]);
        }
    }
}
void search_for_min_stolb_static(int (*a)[NMAX], int *stroki, int *stolb) {
    int new_array[*stolb];
    for (int i = 0; i < *stolb; i++) {
        int min_v = a[0][i];
        for (int j = 0; j < *stroki; j++) {
            if (a[j][i] < min_v) {
                min_v = a[j][i];
            }
        }
        new_array[i] = min_v;
        min_v = a[0][i];
    }
    for (int i = 0; i < *stolb; i++) {
        if (i < *stroki - 1) {
            printf("%d ", new_array[i]);
        } else if (i == *stolb - 1) {
            printf("%d", new_array[i]);
        }
    }
}
