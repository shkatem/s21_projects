
#include <stdio.h>
#include <stdlib.h>

int input(int **matrix, int *n, int *m);
void output(int **matrix, int n, int m);
int sum(int **matrix_first, int **matrix_second, int **matrix_result, int *n_result, int *m_result);
int transpose(int **matrix, int n, int m, int **matrix_result);
int mul(int **matrix_first, int **matrix_second, int **matrix_result, int *n_result, int *m_result,
        int *n_second);
int input_memory(int *m);
int input_nums(int *stroki, int *stolb);

int main() {
    int n1 = 0, n2 = 0, n3 = 0, k1 = 0, k2 = 0, k3 = 0, m = 0;
    int key = 0;
    int *n_first = &n1, *n_second = &n2, *n_result = &n3, *m_first = &k1, *m_second = &k2, *m_result = &k3,
        *memory = &m;
    if (input_memory(memory)) {
        if (input_nums(n_first, m_first)) {
            int q = 0;
            int **matrix_first = malloc((*n_first) * (*m_first) * sizeof(int) + (*n_first) * sizeof(int *));
            int *ptr1 = (int *)(matrix_first + *n_first);
            for (int i = 0; i < *n_first; i++) matrix_first[i] = ptr1 + (*m_first) * i;
            if (input(matrix_first, n_first, m_first)) {
                q = 1;
            }
            if (q) {
                key = *memory;
                switch (key) {
                    case 1: {
                        q = 0;
                        if (input_nums(n_second, m_second)) {
                            int **matrix_second =
                                malloc((*n_second) * (*m_second) * sizeof(int) + (*n_second) * sizeof(int *));
                            int *ptr2 = (int *)(matrix_second + *n_second);
                            for (int i = 0; i < *n_second; i++) matrix_second[i] = ptr2 + (*m_second) * i;
                            if (input(matrix_second, n_second, m_second)) {
                                q = 1;
                            }
                            if (q) {
                                if ((*n_first == *n_second) || (*m_first == *m_second)) {
                                    *n_result = *n_second;
                                    *m_result = *m_second;
                                    int **matrix_result = malloc((*n_result) * (*m_result) * sizeof(int) +
                                                                 (*n_result) * sizeof(int *));
                                    int *ptr3 = (int *)(matrix_result + *n_result);
                                    for (int i = 0; i < *n_result; i++)
                                        matrix_result[i] = ptr3 + (*m_result) * i;
                                    sum(matrix_first, matrix_second, matrix_result, n_result, m_result);
                                    output(matrix_result, *n_result, *m_result);
                                    free(matrix_result);
                                    free(matrix_first);
                                    free(matrix_second);
                                } else {
                                    printf("n/a");
                                }
                            }
                        }
                        break;
                        case 2: {
                            q = 0;
                            if (input_nums(n_second, m_second)) {
                                int **matrix_second = malloc((*n_second) * (*m_second) * sizeof(int) +
                                                             (*n_second) * sizeof(int *));
                                int *ptr2 = (int *)(matrix_second + *n_second);
                                for (int i = 0; i < *n_second; i++) matrix_second[i] = ptr2 + (*m_second) * i;
                                if (input(matrix_second, n_second, m_second)) {
                                    q = 1;
                                }
                                if (q) {
                                    if (*n_second == *m_first) {
                                        *n_result = *n_first;
                                        *m_result = *m_second;
                                        int **matrix_result = malloc((*n_result) * (*m_result) * sizeof(int) +
                                                                     (*n_result) * sizeof(int *));
                                        int *ptr3 = (int *)(matrix_result + *n_result);
                                        for (int i = 0; i < *n_result; i++)
                                            matrix_result[i] = ptr3 + (*m_result) * i;
                                        mul(matrix_first, matrix_second, matrix_result, n_result, m_result,
                                            n_second);
                                        output(matrix_result, *n_result, *m_result);
                                        free(matrix_result);
                                        free(matrix_first);
                                        free(matrix_second);
                                    } else {
                                        printf("n/a");
                                    }
                                }
                            }
                            break;
                        }

                        case 3: {
                            *n_result = *m_first;
                            *m_result = *n_first;
                            int **matrix_result =
                                malloc((*n_result) * (*m_result) * sizeof(int) + (*n_result) * sizeof(int *));
                            int *ptr3 = (int *)(matrix_result + *n_result);
                            for (int i = 0; i < *n_result; i++) matrix_result[i] = ptr3 + (*m_result) * i;
                            transpose(matrix_first, *n_first, *m_first, matrix_result);
                            output(matrix_result, *n_result, *m_result);
                            free(matrix_result);
                            free(matrix_first);
                            break;
                        }
                        default:
                            printf("n/a");
                    }
                }
            }
        }
    }
    return 0;
}

int input(int **matrix, int *n, int *m) {
    int q = 1;
    char probel;
    for (int i = 0; i < *n; i++)
        for (int j = 0; j < *m; j++)
            if ((scanf("%d%c", &(matrix[i][j]), &probel) != 2) || ((probel != '\n') && (probel != ' '))) {
                q = 0;
            }
    if ((!q) || (matrix == NULL)) {
        printf("n/a");
        q = 0;
    }
    return q;
}

int input_memory(int *m) {
    char probel;
    int q = 1;
    if ((scanf("%d%c", m, &probel) != 2) || ((probel != '\n') && (probel != ' ')) || (*m > 4) || (*m <= 0)) {
        printf("n/a");
        q = 0;
    }
    return q;
}

int input_nums(int *stroki, int *stolb) {
    char probel1, probel2;
    int q = 1;
    if ((scanf("%d%c%d%c", stroki, &probel1, stolb, &probel2) != 4) || ((*stolb <= 0) || (*stroki <= 0)) ||
        ((probel1 != '\n') && (probel1 != ' ') && (probel2 != ' ') && (probel2 != '\n'))) {
        printf("n/a");
        q = 0;
    }
    return q;
}

int sum(int **matrix_first, int **matrix_second, int **matrix_result, int *n_result, int *m_result) {
    for (int i = 0; i < *n_result; i++)
        for (int j = 0; j < *m_result; j++) matrix_result[i][j] = matrix_first[i][j] + matrix_second[i][j];
    return 0;
}
void output(int **matrix, int n, int m) {
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
            if (j < m - 1) {
                printf("%d ", matrix[i][j]);
            } else if ((j == m - 1) && (i != n - 1)) {
                printf("%d\n", matrix[i][j]);
            } else if ((j == m - 1) && (i == n - 1)) {
                printf("%d", matrix[i][j]);
            }
}
int transpose(int **matrix, int n, int m, int **matrix_result) {
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++) matrix_result[j][i] = matrix[i][j];
    return 0;
}

int mul(int **matrix_first, int **matrix_second, int **matrix_result, int *n_result, int *m_result,
        int *n_second) {
    for (int i = 0; i < *n_result; ++i)
        for (int j = 0; j < *m_result; ++j)
            for (int k = 0; k < *n_second; ++k)
                matrix_result[i][j] += matrix_first[i][k] * matrix_second[k][j];
    return 1;
}
