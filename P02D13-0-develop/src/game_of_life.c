#include <ncurses.h>
#include <stdio.h>
#include <stdlib.h>

char** alloc(int n, int m);
int check(char** current_matrix, int n, int m);
char** iterate(char** current_matrix, char** next_matrix, int* key);
int output(char** next_matrix);
#define N 25
#define M 80

int main() {
    initscr();
    char** current_matrix = alloc(N, M);
    char** next_matrix = alloc(N, M);
    char ent;
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < M; j++) {
            scanf("%c", &ent);
            if (ent != '\n') {
                current_matrix[i][j] = ent;
            } else {
                j--;
            }
        }
    }

    int key = 1;
    int sp = 200;
    int* speed = &sp;

    while (key) {
        nodelay(stdscr, TRUE);

        char ch = getch();

        if (ch == '1') {
            *speed = 500;
        }
        if (ch == '2') {
            *speed = 250;
        }
        if (ch == '3') {
            *speed = 100;
        }
        if (ch == '4') {
            *speed = 10;
        }
        next_matrix = iterate(current_matrix, next_matrix, &key);
        output(next_matrix);
        refresh();
        napms(*speed);
        clear();
    }
    for (int i = 0; i < N; i++) {
        free(current_matrix[i]);
        free(next_matrix[i]);
    }
    free(current_matrix);
    free(next_matrix);
    nodelay(stdscr, FALSE);
    endwin();
    return 0;
}

char** iterate(char** current_matrix, char** next_matrix, int* key) {
    int q = 0;

    for (int i = 0; i < N; i++) {
        for (int j = 0; j < M; j++) {
            if (!check(current_matrix, i, j)) {
                next_matrix[i][j] = '.';
            } else {
                next_matrix[i][j] = '*';
            }
        }
    }
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < M; j++) {
            if (current_matrix[i][j] != next_matrix[i][j]) {
                q = 1;
            }
            current_matrix[i][j] = next_matrix[i][j];
        }
    }
    *key = q;
    return next_matrix;
}

char** alloc(int n, int m) {
    char** matrix = malloc(n * sizeof(char**));
    for (int i = 0; i < n; i++) {
        matrix[i] = malloc(m * sizeof(char**));
    }
    return matrix;
}

int output(char** next_matrix) {
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < M; j++) {
            printw("%c", next_matrix[i][j]);
        }
        printw("\n");
    }
    return 0;
}

int check(char** current_matrix, int n, int m) {
    int num_neighbors = 0;
    int q = 0;
    if (current_matrix[(n - 1 + N) % N][(m - 1 + M) % M] == '*') {
        num_neighbors++;
    }
    if (current_matrix[(n - 1 + N) % N][(m + M) % M] == '*') {
        num_neighbors++;
    }
    if (current_matrix[(n - 1 + N) % N][(m + 1 + M) % M] == '*') {
        num_neighbors++;
    }
    if (current_matrix[(n + N) % N][(m - 1 + M) % M] == '*') {
        num_neighbors++;
    }
    if (current_matrix[(n + N) % N][(m + 1 + M) % M] == '*') {
        num_neighbors++;
    }
    if (current_matrix[(n + 1 + N) % N][(m - 1 + M) % M] == '*') {
        num_neighbors++;
    }
    if (current_matrix[(n + 1 + N) % N][(m + M) % M] == '*') {
        num_neighbors++;
    }
    if (current_matrix[(n + 1 + N) % N][(m + 1 + M) % M] == '*') {
        num_neighbors++;
    }
    if ((current_matrix[n][m] == '.') && (num_neighbors == 3)) {
        q = 1;
    } else if ((current_matrix[n][m] == '*') && ((num_neighbors == 2) || (num_neighbors == 3))) {
        q = 1;
    } else {
        q = 0;
    }

    return q;
}
