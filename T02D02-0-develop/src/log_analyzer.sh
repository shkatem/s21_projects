echo 'Which file do you want to analyse? Provide root directory:'
read directory

#check if file even exists:
if [ ! -f $directory ]
then
    echo "The file does not exist"
    exit 1
fi


for line in $(awk '{print $1}' "$directory")
do
    if [ ! -f $line ]
    then
        echo "Something is wrong with your log. It seems to be invalid"
        exit 1
    fi
done

### 1: COUNT ALL LINES
wc -l "$directory" | awk '{printf $1} '

#a=$(awk '{print $1}' "$directory" | sort)
#echo $a
### 2: get a first word of each line and count how many unique values are the first word of each line
awk '{print $1}' "$directory" | sort | uniq | wc -l | awk '{printf $1} '


### 3: get an 8th word of each line (=hash) and count how many unique hashes (=number of changes that caused changes in the hash file) MINUS UNIQUE FILES, BECAUSE EACH TIME WE CREATE IT, THERE IS A NULL, WHICH LATER CHANGES:
unique_files=$(awk '{print $1}' "$directory" | sort | uniq | wc -l | awk '{printf $1} ')

unique_hash=$(awk '{print $8}' "$directory" | sort | uniq | wc -l | awk '{print $1}')
echo $(($unique_hash - $unique_files))

#
