echo 'Which file do you want to change? Provide root directory:'
read directory

if [ ! -f $directory ]
then
    echo "The file does not exist"
    exit 1
fi

echo 'Which line do you want to change? :'
read current_line
echo 'Write down the new line:'
read new_line

### EDITING THE FILE ###
sed -i '' "s:"$current_line":"$new_line":g" "$directory"

### ADDING CHANGES TO FILES.LOG ###

#directory name
printf "$directory" >> files.log
printf " - " >> files.log

#size in bytes
wc -c "$directory" | awk '{printf $1}' >> files.log
printf " - " >> files.log

#date in the right format
date_formatted=$(date +'%Y-%m-%d %H:%M')
printf "$date_formatted" >> files.log
printf " - " >> files.log

#sha-sum
shasum -a 256 "$directory" | awk '{ printf $1 }' >> files.log
printf " - " >> files.log

#sha256
echo "sha256" >> files.log
