#include "stack.h"
#include <stdio.h>
#include <stdlib.h>
double stack(char *a, double x0) {
  struct st *STACK = NULL;
  char outstring[1000];
  int k, point;
  k = point = 0;
  while (a[k] != '\0') {
    if (a[k] == ')') {
      while ((STACK->c) != '(') {
        outstring[point++] = pop(&STACK);
        outstring[point++] = ' ';
      }
      pop(&STACK);
    }
    if ((a[k] == 'x' || a[k] == 'y') || (a[k] == '.') ||
        (a[k] >= '0' && a[k] <= '9')) {
      outstring[point++] = a[k];
    }
    if (a[k] == '(') {
      STACK = push(STACK, '(');
      outstring[point++] = ' ';
    }
    if (a[k] == '+' || a[k] == '-' || a[k] == '/' || a[k] == '*' ||
        a[k] == 's' || a[k] == 'c' || a[k] == 't' || a[k] == 'g' ||
        a[k] == 'q' || a[k] == 'l' || a[k] == '~') {
      outstring[point++] = ' ';
      if (STACK == NULL)
        STACK = push(STACK, a[k]);
      else if (PRIOR(STACK->c) < PRIOR(a[k]))
        STACK = push(STACK, a[k]);
      else {
        while ((STACK != NULL) && (PRIOR(STACK->c) >= PRIOR(a[k]))) {
          outstring[point++] = ' ';
          outstring[point++] = pop(&STACK);
        }
        STACK = push(STACK, a[k]);
      }
    }
    k++;
  }
  while (STACK != NULL) {
    outstring[point++] = ' ';
    outstring[point++] = pop(&STACK);
  }

  outstring[point] = '\0';
  double res = get_result(outstring, x0);
  return res;
}

struct st *push(struct st *HEAD, char a) {
  struct st *PTR;
  if ((PTR = malloc(sizeof(struct st))) != NULL) {
    PTR->c = a;
    PTR->next = HEAD;
  }

  return PTR;
}

char pop(struct st **HEAD) {
  char a;
  if (*HEAD == NULL) {
    a = '\0';
  } else {
    struct st *PTR = *HEAD;
    a = PTR->c;
    *HEAD = PTR->next;
    free(PTR);
  }

  return a;
}

int PRIOR(char a) {
  int q = 0;
  switch (a) {
  case '~':
    q = 4;
    break;
  case 's':
  case 'c':
  case 't':
  case 'g':
  case 'q':
  case 'l':
    q = 4;
    break;
  case '*':
  case '/':
    q = 3;
    break;

  case '-':
  case '+':
    q = 2;
    break;

  case '(':
    q = 1;
    break;
  }
  return q;
}
