#include "graph.h"

void risyi(const double *result) {
  char a[N][M];
  for (int i = 0; i < N; i++) {
    for (int j = 0; j < M; j++) {
      a[i][j] = '.';
    }
  }
  for (int j = 0; j < M; j++) {
    for (int i = 0; i < N; i++) {
      int smestica = round(result[j] / (2.0 / (N - 1)));
      if (((13 + smestica) < 25) && ((13 + smestica) >= 0)) {
        a[13 + smestica][j] = '*';
      }
    }
  }
  for (int i = 0; i < N; i++) {
    for (int j = 0; j < M; j++) {
      printf("%c", a[i][j]);
    }
    printf("\n");
  }
}

void get_x_array(double *result) {
  double x = 0;
  for (int j = 0; j < M; j++) {
    result[j] = x;
    x += (4.0 * M_PI / (M - 1));
  }
}
