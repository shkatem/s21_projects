#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct st {
  double c;
  struct st *next;
};
struct st *push_double(struct st *, double a);
char pop_double(struct st **);
int PRIOR(char);
int stack(char *a);
double get_result(char *a, double x0);