#include "get.h"

double get_result(char *a, double x0) {
  double result = 1000000;
  struct st *STACK = NULL;
  for (int k = 0; a[k] != '\0'; k++) {
    if (a[k] == '+' || a[k] == '-' || a[k] == '/' || a[k] == '*' ||
        a[k] == 's' || a[k] == 'c' || a[k] == 't' || a[k] == 'g' ||
        a[k] == 'q' || a[k] == 'l' || a[k] == '~') {
      switch (a[k]) {
      case '+':
        result = STACK->c + STACK->next->c;
        pop_double(&STACK);
        pop_double(&STACK);
        break;
      case '-':
        result = STACK->next->c - STACK->c;
        pop_double(&STACK);
        pop_double(&STACK);
        break;
      case '*':
        result = STACK->c * STACK->next->c;
        pop_double(&STACK);
        pop_double(&STACK);
        break;
      case '/':
        if (STACK->c != 0) {
          result = STACK->next->c / STACK->c;
        }
        pop_double(&STACK);
        pop_double(&STACK);
        break;
      case 's':
        result = sin(STACK->c);
        pop_double(&STACK);
        break;
      case 'c':
        result = cos(STACK->c);
        pop_double(&STACK);
        break;
      case 't':
        if (STACK->c != 0) {
          result = tanh(STACK->c);
        }
        pop_double(&STACK);
        break;
      case 'g':
        if (STACK->c != 0) {
          if (tanh(STACK->c) != 0) {
            result = 1 / tanh(STACK->c);
          } else {
            result = 1 / tanh(STACK->c);
          }
        }
        pop_double(&STACK);
        break;
      case 'q':
        result = sqrt(STACK->c);
        pop_double(&STACK);
        break;
      case 'l':
        result = log(STACK->c);
        pop_double(&STACK);
        break;
      }
      STACK = push_double(STACK, result);
    }

    else if (a[k] != ' ') {
      if (a[k] == 'x') {
        STACK = push_double(STACK, x0);
      } else {
        double temp;
        char *ptr;
        char *d = (char *)malloc(sizeof(char));
        d[0] = a[k];
        int total = 0;
        while ((a[k] != ' ')) {
          void *tmp = (char *)realloc(d, (total++) * sizeof(char));
          if (tmp == NULL) {
            break;
          } else {
            d = tmp;
          }
          d[total - 1] = a[k];
          k++;
        }
        temp = strtod(d, &ptr);
        STACK = push_double(STACK, temp);
        free(d);
      }
    }
  }
  while (STACK != NULL) {
    pop_double(&STACK);
  }
  return result;
}

struct st *push_double(struct st *HEAD, double a) {
  struct st *PTR;
  if ((PTR = malloc(sizeof(struct st))) != NULL) {
    PTR->c = a;
    PTR->next = HEAD;
  }
  return PTR;
}

char pop_double(struct st **HEAD) {
  struct st *PTR;
  double a;
  if (*HEAD == NULL) {
    a = 0;
  } else {
    PTR = *HEAD;
    a = PTR->c;
    *HEAD = PTR->next;
    free(PTR);
  }
  return a;
}
