struct st {
  char c;
  struct st *next;
};
struct st *push(struct st *, char);
char pop(struct st **);
int PRIOR(char);
double stack(char *a, double x0);

double get_result(char *a, double x0);
