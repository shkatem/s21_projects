#include "graph.h"
#include "stack.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *input();
char *lexem_search(char *str);
int main() {
  char *str = input();
  char *str2 = lexem_search(str);
  double temp[M];
  double results_for_graph[M];
  get_x_array(temp);
  for (int j = 0; j < M; j++) {
    results_for_graph[j] = stack(str2, temp[j]);
  }
  risyi(results_for_graph);
  free(str);
  free(str2);
  return 0;
}
char *input() {
  char *str;
  int total = 1;
  str = (char *)malloc(sizeof(char));
  str[0] = getchar();
  while ((str[total - 1] != '\n')) {
    void *tmp = realloc(str, (total++) * sizeof(char));
    if (NULL == tmp) {
      free(str);
    }
    str[total - 1] = getchar();
  }
  str[total] = '\0';
  return str;
}
char *lexem_search(char *str) {
  char *str2 = (char *)malloc((strlen(str)) * sizeof(char));
  int count = 0;
  for (int length = 0; str[length] != '\0'; length++, count++) {
    if ((str[length] == 's') && (str[length + 1] == 'i') &&
        (str[length + 2] == 'n')) {
      str2[count] = 's';
      void *tmp = realloc(str2, (strlen(str) - 2) * sizeof(char));
      if (NULL == tmp) {
        free(str2);
      } else {
        str2 = tmp;
      }
      length++;
      length++;
    } else if ((str[length] == 'c') && (str[length + 1] == 'o') &&
               (str[length + 2] == 's')) {
      str2[count] = 'c';
      void *tmp = realloc(str2, (strlen(str) - 2) * sizeof(char));
      if (NULL == tmp) {
        free(str2);
      } else {
        str2 = tmp;
      }
      length++;
      length++;
    } else if ((str[length] == 't') && (str[length + 1] == 'a') &&
               (str[length + 2] == 'n')) {
      str2[count] = 't';
      void *tmp = realloc(str2, (strlen(str) - 2) * sizeof(char));
      if (NULL == tmp) {
        free(str2);
      } else {
        str2 = tmp;
      }
      length++;
      length++;
    } else if ((str[length] == 'c') && (str[length + 1] == 't') &&
               (str[length + 2] == 'g')) {
      str2[count] = 'g';
      void *tmp = realloc(str2, (strlen(str) - 2) * sizeof(char));
      if (NULL == tmp) {
        free(str2);
      } else {
        str2 = tmp;
      }
      length++;
      length++;
    } else if ((str[length] == 's') && (str[length + 1] == 'q') &&
               (str[length + 2] == 'r') && (str[length + 3] == 't')) {
      str2[count] = 'q';
      void *tmp = realloc(str2, (strlen(str) - 3) * sizeof(char));
      if (NULL == tmp) {
        free(str2);
      } else {
        str2 = tmp;
      }
      length++;
      length++;
      length++;
    } else if ((str[length] == 'l') && (str[length + 1] == 'n')) {
      str2[count] = 'l';
      void *tmp = realloc(str2, (strlen(str) - 1) * sizeof(char));
      if (NULL == tmp) {
        free(str2);
      } else {
        str2 = tmp;
      }
      length++;
    } else {
      str2[count] = str[length];
    }
    str2[count + 1] = '\0';
  }
  return str2;
}