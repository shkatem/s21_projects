#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
    char *s = malloc(sizeof(char));
    int total = 0;
    scanf("%c", &s[0]);
    while ((s[total] != '\n')) {
        void *tmp = realloc(s, sizeof(char));
        if (NULL == tmp) {
            free(s);
        } else {
            s = tmp;
        }
        scanf("%c", &s[total + 1]);
        total ++;
    }
    s[total] = '\0';
    printf("%s", s);
    free(s);
    return 1;
}