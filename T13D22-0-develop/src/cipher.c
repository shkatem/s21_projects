#include <ctype.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int get_variant();
void read(char *s, int *file_not_found);
void write(char *s, int *file_not_found);
char *caesar(char *text);
void get_files();
void encrypt_c(char *filename);
void clear_h(char *filename);

int main() {
    int f = 0;
    int *file_not_found = &f;
    int c;
    char *s = (char *)malloc(sizeof(char *));
    do {
        c = get_variant();
        if (c == 1) {
            read(s, file_not_found);
        } else if (c == 2) {
            write(s, file_not_found);
        } else if (c == 3) {
            get_files();
        }
    } while (c != -1);
    free(s);
    return 1;
}

int get_variant() {
    int variant;
    char b;
    int m;
    m = scanf("%d%c", &variant, &b);
    while (m != 2 || ((variant != 1) && (variant != -1) && (variant != 2) && (variant != 3))) {
        printf("n/a\n");
        fflush(stdin);
        m = scanf("%d%c", &variant, &b);
    }
    return variant;
}

void read(char *s, int *file_not_found) {
    char *read_string = (char *)malloc(sizeof(char *));
    int total = 0, total2 = 0;
    scanf("%c", &s[0]);
    while ((s[total - 1] != '\n')) {
        void *tmp = realloc(s, (total++) * sizeof(char *));
        if (NULL == tmp) {
            free(s);
        } else {
            s = tmp;
        }
        scanf("%c", &s[total - 1]);
    }
    s[total - 1] = '\0';
    FILE *file;
    file = fopen(s, "r");
    int q = 0;
    char ch;
    if ((file != NULL)) {
        while (fscanf(file, "%c", &ch) != EOF) {
            void *tmp2 = realloc(read_string, (total2++) * sizeof(char));
            if (NULL == tmp2) {
                free(read_string);
            } else {
                read_string = tmp2;
            }
            read_string[total2 - 1] = ch;
            q = 1;
        }
        if (!q) {
            printf("n/a");
        }
        read_string[total2] = '\0';
        printf("%s\n", read_string);
        fclose(file);
    } else {
        printf("n/a\n");
        if (!q) {
            *file_not_found = 1;
        }
    }
    free(read_string);
}

void write(char *s, int *file_not_found) {
    char *write_string = (char *)malloc(sizeof(char));
    int total2 = 1;
    int ch;
    FILE *file;
    if (*file_not_found) {
        scanf("%c", &write_string[0]);
    } else {
        if ((file = fopen(s, "a")) != NULL) {
            scanf("%c", &write_string[0]);
            while ((write_string[total2 - 1] != '\n')) {
                void *tmp3 = realloc(write_string, (total2++) * sizeof(char));
                if (NULL == tmp3) {
                    free(write_string);
                } else {
                    write_string = tmp3;
                }
                scanf("%c", &write_string[total2 - 1]);
            }
            write_string[total2 - 1] = '\0';
            fprintf(file, "%s", write_string);
            fclose(file);
            file = fopen(s, "r");
            while ((ch = fgetc(file)) != EOF) {
                printf("%c", ch);
            }
            printf("\n");
            fclose(file);
        }
    }
    free(write_string);
}
void get_files() {
    DIR *d;
    // char *g = (char *)malloc(sizeof(char));
    struct dirent *dir;
    // int total =1;
    // scanf("%c", &s[0]);
    // while ((s[total - 1] != '\n')) {
    //     void *tmp6 = realloc(s, (total++) * sizeof(char));
    //     if (NULL == tmp6) {
    //         free(s);
    //     } else {
    //         s = tmp6;
    //     }
    //     scanf("%c", &s[total - 1]);
    // }
    // s[total - 1] = '\0';
    // char* my_dir = (char *)malloc((strlen(s)+1)*sizeof(char));
    char *my_dir = "../src/ai_modules/";
    char *temp_file = (char *)malloc((strlen(my_dir)) * sizeof(char));
    // strcpy(my_dir, s);
    strcpy(temp_file, my_dir);
    // strcat(my_dir, "/");
    d = opendir(my_dir);
    if (d) {
        while ((dir = readdir(d)) != NULL) {
            //    printf("%s\n", dir->d_name);
            strcpy(temp_file, my_dir);
            // printf("%s\n", temp_file);
            temp_file = realloc(temp_file, (strlen(dir->d_name) + strlen(my_dir) + 1) * sizeof(char));
            //    strcat(temp_file, "/");
            strcat(temp_file, dir->d_name);
            if (temp_file[(strlen(dir->d_name) + strlen(my_dir)) - 1] == 'c') {
                encrypt_c(temp_file);
            }
            if (temp_file[(strlen(dir->d_name) + strlen(my_dir)) - 1] == 'h') {
                clear_h(temp_file);
            }
            // printf("%s\n", temp_file);
            // temp_file = NULL;
        }
        closedir(d);
    }
    // free(my_dir);
    free(temp_file);
    printf("Modules are now changed\n");

    // char *temp = (char *)malloc(sizeof(char));
    // int length = 1;
    // int ch;
}

void encrypt_c(char *filename) {
    char *encrypting = (char *)malloc(sizeof(char));
    int total2 = 0;
    FILE *file3;
    file3 = fopen(filename, "r");
    int q = 0;
    char ch;
    if ((file3 != NULL)) {
        while (fscanf(file3, "%c", &ch) != EOF) {
            void *tmp7 = realloc(encrypting, (total2++) * sizeof(char));
            if (NULL == tmp7) {
                free(encrypting);
            } else {
                encrypting = tmp7;
            }
            encrypting[total2 - 1] = ch;
            q = 1;
        }
        if (!q) {
            printf("n/a");
        }
        encrypting[total2] = '\0';
        // printf("%s\n", encrypting);
        fclose(file3);
        encrypting = caesar(encrypting);
        file3 = fopen(filename, "w");
        fprintf(file3, "%s", encrypting);
        fclose(file3);
    } else {
        printf("n/a\n");
    }
    free(encrypting);
}

char *caesar(char *text) {
    char ch;
    int key = 6;
    //   scanf("%d", & key);
    for (int i = 0; text[i] != '\0'; ++i) {
        ch = text[i];
        if (isalnum(ch)) {
            if (islower(ch)) {
                ch = (ch - 'a' + key) % 26 + 'a';
            }
            if (isupper(ch)) {
                ch = (ch - 'A' + key) % 26 + 'A';
            }
            if (isdigit(ch)) {
                ch = (ch - '0' + key) % 10 + '0';
            }
        }
        text[i] = ch;
    }
    //   printf("Encrypted message: %s", text);
    return text;
}
void clear_h(char *filename) {
    FILE *file4;
    file4 = fopen(filename, "w+");
    fclose(file4);
}
