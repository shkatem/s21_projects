
#include <stdio.h>
#define NMAX 10

int input(int *buffer, int *length);
void output(int *number, int *numbers);
int sum_numbers(int *buffer, int *length);
int find_numbers(int *buffer, int *length, int n1, int *numbers);

int main() {
    int n, n1 = 0, q = 1;
    int *length = &n;
    int *number = &n1;
    int buffer[NMAX], numbers[NMAX];
    if (input(buffer, length)) {
        if (sum_numbers(buffer, length) == -1) {
            printf("n/a");
            q = 0;
        } else {
            printf("%d\n", sum_numbers(buffer, length));
        }
        if (q) {
            *number = find_numbers(buffer, length, n1, numbers);
            output(number, numbers);
        }
    }
    return 1;
}

int input(int *buffer, int *length) {
    char probel;
    int q = 1;
    if ((scanf("%d%c", length, &probel) != 2) || (probel != '\n') || (*length > NMAX) || (*length <= 0)) {
        printf("n/a");
        q = 0;
    }
    if (q != 0) {
        int num_even = 0;
        for (int i = 0; i <= *length; i++) {
            if (i < (*length - 1)) {
                if ((scanf("%d%c", &buffer[i], &probel) != 2) || ((probel != '\n') && (probel != ' '))) {
                    printf("n/a");
                    q = 0;
                    break;
                }
            } else if (i == (*length - 1)) {
                if ((scanf("%d%c", &buffer[i], &probel) != 2) || ((probel != '\n'))) {
                    printf("n/a");
                    q = 0;
                    break;
                }
            }
        }
    }
    return q;
}

int sum_numbers(int *buffer, int *length) {
    int sum = 0, q = 0;

    for (int i = 0; i < *length; i++) {
        if (buffer[i] % 2 == 0) {
            sum = sum + buffer[i];
            q = 1;
        }
    }
    if (!q) {
        sum = -1;
    }
    return sum;
}

int find_numbers(int *buffer, int *length, int n1, int *numbers) {
    for (int i = 0; i < *length; i++) {
        if (buffer[i] != 0) {
            if (sum_numbers(buffer, length) % buffer[i] == 0) {
                numbers[n1] = buffer[i];
                n1++;
            }
        }
    }
    return n1;
}

void output(int *number, int *numbers) {
    for (int i = 0; i < *number; i++) {
        if (i != *number - 1) printf("%d ", numbers[i]);
        if (i == *number - 1) printf("%d", numbers[i]);
    }
}
