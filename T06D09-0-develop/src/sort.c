#include <stdio.h>
#define N 10

int input(int *a);
void quick_sort(int *a, int first, int last);
void swap(int *xp, int *yp);
void printArray(int *a);

int main() {
    int data[N];
    int q = 1;
    if (input(data)) {
        quick_sort(data, 0, N - 1);
        printArray(data);
    }
    return 0;
}

int input(int *a) {
    char probel;
    int q = 1;
    //    million checks for a right format
    for (int i = 0; i <= N; i++) {
        if (i < (N - 1)) {
            if ((scanf("%d%c", &a[i], &probel) != 2) || ((probel != '\n') && (probel != ' '))) {
                printf("n/a");
                q = 0;
                break;
            }
        } else if (i == (N - 1)) {
            if ((scanf("%d%c", &a[i], &probel) != 2) || ((probel != '\n'))) {
                printf("n/a");
                q = 0;
                break;
            }
        }
    }
    return q;
}

void quick_sort(int *a, int first, int last) {
    if (first < last) {
        int left = first, right = last, middle = a[(left + right) / 2];
        while (left <= right) {
            while (a[left] < middle) left++;
            while (a[right] > middle) right--;
            if (left <= right) {
                swap(a + left, a + right);
                left++;
                right--;
            }
        }
        quick_sort(a, first, right);
        quick_sort(a, left, last);
    }
}
void swap(int *xp, int *yp) {
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}

void printArray(int *a) {
    for (int i = 0; i < N; ++i) {
        if (i != N - 1) printf("%d ", a[i]);
        if (i == N - 1) printf("%d", a[i]);
    }
}
