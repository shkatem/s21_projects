#include <stdio.h>

#define LEN 100

void sum(int *buff1, int *len1, int *buff2, int *len2, int *result, int *result_length);
void sub(int *buff1, int *len1, int *buff2, int *len2, int *result, int *result_length);
int input(int *buff1, int *len1, int *buff2, int *len2);
int make_ints(int *buff1, int *len1, int *buff2, int *len2);
// void populate_zeros(int *buff2, int *len2, int *len1);
//  void output(int *buff2, int *len1);
//  void format_second_array(int *buff2, int *len2, int *len1);

int main() {
    int len11 = 0, len22 = 0, len_res = 0;
    int *len1 = &len11, *len2 = &len22, *result_length = &len_res;
    int buff1[LEN], buff2[LEN], result[*result_length];
    if (input(buff1, len1, buff2, len2)) {
        //    populate_zeros(buff2, len2, len1);
        sum(buff1, len1, buff2, len2, result, result_length);
        result[*result_length] = 0;
        sub(buff1, len1, buff2, len2, result, result_length);
    }
    //    printf("%d\n", *result_length);
    //    output(buff2, len1);
    return 0;
}

int input(int *buff1, int *len1, int *buff2, int *len2) {
    char probel = ' ';
    int q = 1, l1 = 0, l2 = 0;
    //    million checks for a right format
    for (int i = 0; i <= LEN; i++) {
        if (probel != '\n') {
            l1 = i + 1;
            if ((scanf("%d%c", &buff1[i], &probel) != 2) || ((probel != '\n') && (probel != ' '))) {
                printf("n/a");
                q = 0;
                break;
            }
        }
    }
    probel = ' ';
    if (q) {
        for (int j = 0; j <= LEN; j++) {
            if (probel != '\n') {
                l2 = j + 1;
                if ((scanf("%d%c", &buff2[j], &probel) != 2) || ((probel != '\n') && (probel != ' '))) {
                    printf("n/a");
                    q = 0;
                    break;
                }
            }
        }
    }
    *len1 = l1;
    *len2 = l2;
    if ((q) && (l1 < l2)) {
        printf("n/a");
        q = 0;
    }
    return q;
}

void sum(int *buff1, int *len1, int *buff2, int *len2, int *result, int *result_length) {
    *result_length = *len1;
    for (int i = *len1 - 1; i >= 0; i--) {
        if (*len1 - *len2 <= i) {
            result[i] = (buff1[i] + buff2[i - (*len1 - *len2)]) % 10;
            result[i - 1] = (buff1[i] + buff2[i - (*len1 - *len2)]) / 10;
        } else {
            result[i] = buff1[i];
        }
    }

    for (int i = 0; i < *result_length; i++) {
        if (i != *result_length - 1) printf("%d ", result[i]);
        if (i == *result_length - 1) printf("%d\n", result[i]);
        //        result[i] =0;
    }
}

// void populate_zeros(int *buff2, int *len2, int *len1) {
//     for (int i = *len1; i <= *len2 +1; i--){
//         buff2[i-1] = 0;}
//     int pos = *len1 - *len2;
//     int temp;
//     while (pos) {
//         temp = buff2[*len1 - 1];
//         for (int i = *len1 - 1; i >0; i--) {
//             buff2[i] = buff2[i - 1];
//             buff2[0] = temp;
//             pos--;}
//     } }

void sub(int *buff1, int *len1, int *buff2, int *len2, int *result, int *result_length) {
    *result_length = *len1;
    for (int i = *len1 - 1; i >= 0; i--) {
        if (*len1 - *len2 <= i) {
            result[i] = (buff1[i] - buff2[i - (*len1 - *len2)]) % 10;
            result[i - 1] = (buff1[i] - buff2[i - (*len1 - *len2)]) / 10;
        } else {
            result[i] = buff1[i];
        }
    }

    for (int i = 0; i < *result_length; i++) {
        if (i != *result_length - 1) printf("%d ", result[i]);
        if (i == *result_length - 1) printf("%d\n", result[i]);
        //        result[i] =0;
    }
}

// void sum(int *buff1, int* len1, int *buff2, int* len2, int *result, int *result_length) {
//     *result_length = *len1;
//     int k= *len2;
//     for (int m = *len1 -1; m > 0; m--) {
//         k = k- 1;
//         result[m] = buff1[m] + buff2[k];
//     }
// }

// void output(int *buff2, int *len1) {
//     for (int i = 0; i < *len1; i++) {
//         if (i != *len1 - 1) printf("%d ", buff2[i]);
//         if (i == *len1 - 1) printf("%d", buff2[i]);
//     }
// }
