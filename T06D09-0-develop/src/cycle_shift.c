#include <stdio.h>
#define NMAX 10

int input(int *a, int *length, int *change);
void output(int *a, int *length);
void move(int *a, int *length, int *change);

int main() {
    int n, c = 0;
    int *length = &n, *change = &c;
    int data[NMAX];
    if (input(data, length, change)) {
        move(data, length, change);
        output(data, length);
    }
}

int input(int *a, int *length, int *change) {
    char probel;
    int q = 1;
    //    million checks for a right format
    if ((scanf("%d%c", length, &probel) != 2) || (probel != '\n') || (*length > NMAX) || (*length <= 0)) {
        printf("n/a");
        q = 0;
    }
    if (q != 0) {
        for (int i = 0; i <= *length; i++) {
            if (i < (*length - 1)) {
                if ((scanf("%d%c", &a[i], &probel) != 2) || ((probel != '\n') && (probel != ' '))) {
                    printf("n/a");
                    q = 0;
                    break;
                }
            } else if (i == (*length - 1)) {
                if ((scanf("%d%c", &a[i], &probel) != 2) || ((probel != '\n'))) {
                    printf("n/a");
                    q = 0;
                    break;
                }
            }
        }
    }
    if ((scanf("%d%c", change, &probel) != 2) || (probel != '\n')) {
        printf("n/a");
        q = 0;
    }
    return q;
}

void move(int *a, int *length, int *change) {
    int temp;
    int pos = *change;
    while (pos) {
        if (*change >= 0) {
            temp = a[0];
            for (int i = 0; i < *length - 1; i++) a[i] = a[i + 1];

            a[*length - 1] = temp;
            pos--;
        } else if (*change < 0) {
            temp = a[*length - 1];
            for (int i = *length - 1; i > 0; i--) a[i] = a[i - 1];

            a[0] = temp;
            pos++;
        }
    }
}

void output(int *a, int *length) {
    for (int i = 0; i < *length; i++) {
        if (i != *length - 1) printf("%d ", a[i]);
        if (i == *length - 1) printf("%d", a[i]);
    }
}
