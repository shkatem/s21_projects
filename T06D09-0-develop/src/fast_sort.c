#include <stdio.h>
#define N 10

int input(int *a);
void quick_sort(int *a, int first, int last);
void swap(int *xp, int *yp);
void printArrays(int *a, int *b);
void copyArray(int *a, int *copy);
// void bubble_sort(int* a);
void heapSort(int *b);

int main() {
    int data[N], copy[N];
    if (input(data)) {
        copyArray(data, copy);
        quick_sort(data, 0, N - 1);
        //    bubble_sort(copy);
        heapSort(copy);
        printArrays(data, copy);
    }
    return 0;
}

int input(int *a) {
    char probel;
    int q = 1;
    //    million checks for a right format
    for (int i = 0; i <= N; i++) {
        if (i < (N - 1)) {
            if ((scanf("%d%c", &a[i], &probel) != 2) || ((probel != '\n') && (probel != ' '))) {
                printf("n/a");
                q = 0;
                break;
            }
        } else if (i == (N - 1)) {
            if ((scanf("%d%c", &a[i], &probel) != 2) || ((probel != '\n'))) {
                printf("n/a");
                q = 0;
                break;
            }
        }
    }
    return q;
}

void quick_sort(int *a, int first, int last) {
    if (first < last) {
        int left = first, right = last, middle = a[(left + right) / 2];
        while (left <= right) {
            while (a[left] < middle) left++;
            while (a[right] > middle) right--;
            if (left <= right) {
                swap(a + left, a + right);
                left++;
                right--;
            }
        }
        quick_sort(a, first, right);
        quick_sort(a, left, last);
    }
}

void heapify(int *b, int n, int i) {
    int largest = i;
    int left = 2 * i + 1;
    int right = 2 * i + 2;

    if ((left < n) && (b[left] > b[largest])) {
        largest = left;
    }
    if ((right < n) && (b[right] > b[largest])) {
        largest = right;
    }
    if (largest != i) {
        swap(&b[i], &b[largest]);
        heapify(b, n, largest);
    }
}

void heapSort(int *b) {
    //    building a Max-heap
    for (int i = N / 2 - 1; i >= 0; i--) {
        heapify(b, N, i);
    }

    for (int i = N - 1; i >= 0; i--) {
        swap(&b[0], &b[i]);
        heapify(b, i, 0);
    }
}

// void bubble_sort(int* a) {
//     for (int i = 0; i < N; i++) {
//         for (int j = 0; j < N - i; j++) {
//             if (a[j] > a[j+1]) {
//                 swap(a+j, a+j+1);
//             }
//         }
//     }
// }

void swap(int *xp, int *yp) {
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}

void printArrays(int *a, int *b) {
    for (int i = 0; i < N; ++i) {
        if (i != N - 1) printf("%d ", a[i]);
        if (i == N - 1) printf("%d\n", a[i]);
    }
    for (int i = 0; i < N; ++i) {
        if (i != N - 1) printf("%d ", b[i]);
        if (i == N - 1) printf("%d", b[i]);
    }
}

void copyArray(int *a, int *copy) {
    // loop to iterate through array
    for (int i = 0; i < N; ++i) {
        copy[i] = a[i];
    }
}
