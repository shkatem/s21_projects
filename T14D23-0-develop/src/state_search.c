#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct my_struct {
    int year, month, day, hour, minute, second, status, code;
} my_struct;

struct my_struct read_record_from_file(FILE *pfile, int index);
int get_file_size_in_bytes(FILE *pfile);
int get_records_count_in_file(FILE *pfile);
int search(FILE *pfile, char *m);
struct my_struct get_num_from_str(char *m);

char *input(char *s);
int get_variant();
int open(char *s);

int main() {
    char *s = (char *)malloc(sizeof(char));
    input(s);
    FILE *f;
    int result = -1;
    f = fopen(s, "rb");
    if (f == NULL) {
        printf("n/a");
    } else {
        char *m = (char *)malloc(sizeof(char));
        input(m);
        result = search(f, m);
        if (result == -1) {
            printf("n/a");
        } else {
            printf("%d", result);
        }
        fclose(f);
        free(m);
    }
    free(s);

    return 1;
}

struct my_struct get_num_from_str(char *m) {
    char *ptr;
    char day[3];
    char month[3];
    char year[5];
    my_struct user;
    strncpy(day, m + 0, 2);
    strncpy(month, m + 3, 2);
    strncpy(year, m + 6, 4);
    user.day = strtod(day, &ptr);
    user.month = strtod(month, &ptr);
    user.year = strtod(year, &ptr);
    return user;
}

int search(FILE *pfile, char *m) {
    int res = -1;
    my_struct buf;
    my_struct user = get_num_from_str(m);
    int indices = get_records_count_in_file(pfile);
    for (int index = 0; index < indices; index++) {
        buf = read_record_from_file(pfile, index);
        if ((user.year == buf.year) && (user.day == buf.day) && (user.month == buf.month)) {
            res = buf.code;
            break;
        }
    }
    return res;
}

char *input(char *s) {
    int total = 1;
    scanf("%c", &s[0]);
    while ((s[total - 1] != '\n')) {
        void *tmp = realloc(s, (total++) * sizeof(char));
        if (NULL == tmp) {
            free(s);
        } else {
            s = tmp;
        }
        scanf("%c", &s[total - 1]);
    }
    s[total - 1] = '\0';
    return s;
}

// Function of reading a record of a given type from a file by its serial number.
struct my_struct read_record_from_file(FILE *pfile, int index) {
    // Calculation of the offset at which desired entry should be located from the beginning of the file.
    int offset = index * sizeof(struct my_struct);
    // Move the position pointer to the calculated offset from the beginning of the file.
    fseek(pfile, offset, SEEK_SET);

    // Reading a record of the specified type from a file.
    struct my_struct record;
    fread(&record, sizeof(struct my_struct), 1, pfile);

    // For safety reasons, we return the file position pointer to the beginning of the file.
    rewind(pfile);

    // Return read record
    return record;
}

// Function to get file size in bytes.
int get_file_size_in_bytes(FILE *pfile) {
    int size = 0;
    fseek(pfile, 0, SEEK_END);  // Move the position pointer to the end of the file.
    size = ftell(
        pfile);  // Read the number of bytes from the beginning of the file to the current position pointer.
    rewind(pfile);  // For safety reasons, move the position pointer back to the beginning of the file.
    return size;
}

// Function to get count of records in file
int get_records_count_in_file(FILE *pfile) {
    return get_file_size_in_bytes(pfile) / sizeof(struct my_struct);
}
