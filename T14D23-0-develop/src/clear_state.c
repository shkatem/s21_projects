#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct my_struct {
    int year, month, day, hour, minute, second, status, code;
} my_struct;

typedef struct the_search_nums {
    int num1, num2;
} the_search_nums;

typedef struct my_struct_user {
    int year1, month1, day1, year2, month2, day2;
} my_struct_user;

struct my_struct read_record_from_file(FILE *pfile, int index);
void write_record_in_file(FILE *pfile, struct my_struct *record_to_write, int index);
void swap_records_in_file(FILE *pfile, int record_index1, int record_index2);
int get_file_size_in_bytes(FILE *pfile);
int get_records_count_in_file(FILE *pfile);
void just_print(FILE *pfile);
void sort_and_print(FILE *pfile);
void write_and_print(FILE *pfile);
the_search_nums search(FILE *pfile, char *m);
struct my_struct_user get_num_from_str(char *m);
int deleterecords(FILE *pfile, char *s, char *m);
void sort_no_print(FILE *pfile);

char *input(char *s);
int get_variant();
int open(char *s);

int main() {
    char *s = (char *)malloc(sizeof(char));
    input(s);
    FILE *f;
    // int initial_size, ending_size;
    // int result = -1;
    f = fopen(s, "rb");
    if (f == NULL) {
        printf("n/a");
    } else {
        char *m = (char *)malloc(sizeof(char));
        input(m);
        sort_no_print(f);
        // initial_size = get_file_size_in_bytes(f);
        int k = deleterecords(f, s, m);
        if (k) {
            printf("n/a");
        } else {
            f = fopen(s, "rb");
            sort_and_print(f);
            // ending_size = get_file_size_in_bytes(f);
            // printf("%d %d", initial_size, ending_size);
            // if (result == -1) {
            //     printf("n/a");
            // } else {
            //     printf("%d", result);
            // }
            fclose(f);
        }
        free(m);
    }
    free(s);

    return 1;
}

int deleterecords(FILE *pfile, char *s, char *m) {
    int q = 0;
    FILE *fp_tmp;
    fp_tmp = fopen("tmp.bin", "wb");
    my_struct myrecord;
    the_search_nums res = search(pfile, m);
    if ((res.num1 == -1) || (res.num2 == -1)) {
        q = 1;
    }
    int indices = get_records_count_in_file(pfile);
    for (int index = 0; index < indices; index++) {
        myrecord = read_record_from_file(pfile, index);
        if ((index > res.num1) && (index < res.num2)) {
        } else {
            fwrite(&myrecord, sizeof(my_struct), 1, fp_tmp);
        }
    }

    fclose(pfile);
    fclose(fp_tmp);

    remove(s);
    rename("tmp.bin", s);
    return q;
}

my_struct_user get_num_from_str(char *m) {
    char *ptr;
    char day1[3];
    char month1[3];
    char year1[5];
    char day2[3];
    char month2[3];
    char year2[5];
    my_struct_user user;
    strncpy(day1, m + 0, 2);
    strncpy(month1, m + 3, 2);
    strncpy(year1, m + 6, 4);
    strncpy(day2, m + 11, 2);
    strncpy(month2, m + 14, 2);
    strncpy(year2, m + 17, 4);
    user.day1 = strtod(day1, &ptr);
    user.month1 = strtod(month1, &ptr);
    user.year1 = strtod(year1, &ptr);
    user.day2 = strtod(day2, &ptr);
    user.month2 = strtod(month2, &ptr);
    user.year2 = strtod(year2, &ptr);
    return user;
}

the_search_nums search(FILE *pfile, char *m) {
    the_search_nums temp;
    temp.num1 = -1;
    temp.num2 = -1;
    my_struct buf;
    my_struct_user user = get_num_from_str(m);
    int indices = get_records_count_in_file(pfile);
    for (int index = 0; index < indices; index++) {
        buf = read_record_from_file(pfile, index);
        if ((user.year1 == buf.year) && (user.day1 == buf.day) && (user.month1 == buf.month)) {
            temp.num1 = index;
            break;
        }
    }
    for (int index = 0; index < indices; index++) {
        buf = read_record_from_file(pfile, index);
        if ((user.year2 == buf.year) && (user.day2 == buf.day) && (user.month2 == buf.month)) {
            temp.num2 = index;
            break;
        }
    }
    return temp;
}

void just_print(FILE *pfile) {
    my_struct buf;
    int indices = get_records_count_in_file(pfile);
    for (int index = 0; index < indices; index++) {
        buf = read_record_from_file(pfile, index);
        printf("%d %d %d %d %d %d %d %d \n", buf.year, buf.month, buf.day, buf.hour, buf.minute, buf.second,
               buf.status, buf.code);
    }
}

void write_and_print(FILE *pfile) {
    my_struct buf;
    char b;
    scanf("%d%c%d%c%d%c%d%c%d%c%d%c%d%c%d%c", &buf.year, &b, &buf.month, &b, &buf.day, &b, &buf.hour, &b,
          &buf.minute, &b, &buf.second, &b, &buf.status, &b, &buf.code, &b);
    write_record_in_file(pfile, &buf, 0);
    sort_and_print(pfile);
}

void sort_no_print(FILE *pfile) {
    my_struct buf1, buf2;
    int indices = get_records_count_in_file(pfile);
    for (int i = 0; i < indices; i++) {
        for (int j = 0; j < indices - i - 1; j++) {
            buf1 = read_record_from_file(pfile, j);
            buf2 = read_record_from_file(pfile, j + 1);
            unsigned int buf1_in_secs = (buf1.year - 1900) * (3.154e+7) + (2.628e+6) * buf1.month +
                                        86400 * buf1.day + 3600 * buf1.hour + 60 * buf1.minute + buf1.second;
            unsigned int buf2_in_secs = (buf2.year - 1900) * (3.154e+7) + (2.628e+6) * buf2.month +
                                        86400 * buf2.day + 3600 * buf2.hour + 60 * buf2.minute + buf2.second;
            if (buf1_in_secs > buf2_in_secs) {
                swap_records_in_file(pfile, j, j + 1);
            }
        }
    }
}

void sort_and_print(FILE *pfile) {
    my_struct buf1, buf2;
    int indices = get_records_count_in_file(pfile);
    for (int i = 0; i < indices; i++) {
        for (int j = 0; j < indices - i - 1; j++) {
            buf1 = read_record_from_file(pfile, j);
            buf2 = read_record_from_file(pfile, j + 1);
            unsigned int buf1_in_secs = (buf1.year - 1900) * (3.154e+7) + (2.628e+6) * buf1.month +
                                        86400 * buf1.day + 3600 * buf1.hour + 60 * buf1.minute + buf1.second;
            unsigned int buf2_in_secs = (buf2.year - 1900) * (3.154e+7) + (2.628e+6) * buf2.month +
                                        86400 * buf2.day + 3600 * buf2.hour + 60 * buf2.minute + buf2.second;
            if (buf1_in_secs > buf2_in_secs) {
                swap_records_in_file(pfile, j, j + 1);
            }
        }
    }
    just_print(pfile);
}

char *input(char *s) {
    int total = 1;
    scanf("%c", &s[0]);
    while ((s[total - 1] != '\n')) {
        void *tmp = realloc(s, (total++) * sizeof(char));
        if (NULL == tmp) {
            free(s);
        } else {
            s = tmp;
        }
        scanf("%c", &s[total - 1]);
    }
    s[total - 1] = '\0';
    return s;
}

// Function of reading a record of a given type from a file by its serial number.
struct my_struct read_record_from_file(FILE *pfile, int index) {
    // Calculation of the offset at which desired entry should be located from the beginning of the file.
    int offset = index * sizeof(struct my_struct);
    // Move the position pointer to the calculated offset from the beginning of the file.
    fseek(pfile, offset, SEEK_SET);

    // Reading a record of the specified type from a file.
    struct my_struct record;
    fread(&record, sizeof(struct my_struct), 1, pfile);

    // For safety reasons, we return the file position pointer to the beginning of the file.
    rewind(pfile);

    // Return read record
    return record;
}

// Function of writing a record of the specified type to the file at the specified serial number.
void write_record_in_file(FILE *pfile, struct my_struct *record_to_write, int index) {
    // Calculation of the offset at which the required record should be located from the beginning of the
    // file.
    int offset = index * sizeof(struct my_struct);
    // Move the position pointer to the calculated offset from the beginning of the file.
    fseek(pfile, offset, SEEK_SET);

    // Write a record of the specified type to a file.
    fwrite(record_to_write, sizeof(struct my_struct), 1, pfile);

    // Just in case, force the I/O subsystem to write the contents of its buffer to a file right now.
    fflush(pfile);

    // For safety reasons, return the file position pointer to the beginning of the file.
    rewind(pfile);
}

// Function of mutual transfer of two records in the file by their indexes.
void swap_records_in_file(FILE *pfile, int record_index1, int record_index2) {
    // Read both records from file to variables
    struct my_struct record1 = read_record_from_file(pfile, record_index1);
    struct my_struct record2 = read_record_from_file(pfile, record_index2);

    // Rewrite records in file
    write_record_in_file(pfile, &record1, record_index2);
    write_record_in_file(pfile, &record2, record_index1);
}

// Function to get file size in bytes.
int get_file_size_in_bytes(FILE *pfile) {
    int size = 0;
    fseek(pfile, 0, SEEK_END);  // Move the position pointer to the end of the file.
    size = ftell(
        pfile);  // Read the number of bytes from the beginning of the file to the current position pointer.
    rewind(pfile);  // For safety reasons, move the position pointer back to the beginning of the file.
    return size;
}

// Function to get count of records in file
int get_records_count_in_file(FILE *pfile) {
    return get_file_size_in_bytes(pfile) / sizeof(struct my_struct);
}
