#include "stack.h"

int main() {
    struct node* a = init(1);
    push(&a, 2);
    push_test(push(&a, 3));
    push(&a, 4);
    pop_test(pop(&a));
    destroy(a);
    return 0;
}

struct node* init(int num) {
    struct node* head = (struct node*)malloc(sizeof(struct node));
    head->val = num;
    head->next = NULL;
    return head;
}
int push(struct node** root, int num) {
    struct node* temp;
    temp = (struct node*)malloc(sizeof(struct node));
    temp->val = num;
    temp->next = *root;
    (*root) = temp;
    return ((*root)->val);
}

int pop(struct node** root) {
    int num = (*root)->val;
    struct node* temp;
    temp = *root;
    *root = (*root)->next;
    free(temp);
    return (num);
}

void destroy(struct node* root) {
    while (root != NULL) {
        struct node* temp;
        temp = root;
        root = root->next;
        free(temp);
    }
}