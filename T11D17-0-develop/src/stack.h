#include <stdio.h>
#include <stdlib.h>

struct node {
    int val;
    struct node* next;
};

struct node* init(int num);
int push(struct node** root, int num);
int pop(struct node** root);
void destroy(struct node* root);
void pop_test(int num);
void push_test(int num);