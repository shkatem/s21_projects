#include "list.h"

int main() {
    struct door door1 = {1, 0};
    struct node* a = init(&door1);
    struct door door2 = {2, 0};
    add_door(a, &door2);
    struct door door3 = {3, 0};
    add_door(a->next, &door3);
    struct door door4 = {4, 0};
    add_door(a->next->next, &door4);
    add_door_test((find_door(4, a)->val));
    remove_door(a->next->next, a);
    remove_door_test(find_door(3, a));
    destroy(a);
    return 0;
}

struct node* init(struct door* door)  // значение первого узла
{
    // выделение памяти под корень списка
    struct node* head = (struct node*)malloc(sizeof(struct node));
    head->val = door->id;
    head->status = door->status;
    head->next = NULL;  // это последний узел списка
    return head;
}
struct node* add_door(struct node* elem, struct door* door) {
    struct node *temp, *p;
    temp = (struct node*)malloc(sizeof(struct node));
    p = elem->next;        // сохранение указателя на следующий узел
    elem->next = temp;     // предыдущий узел указывает на создаваемый
    temp->val = door->id;  // сохранение поля данных добавляемого узла
    temp->status = door->status;  // сохранение поля данных добавляемого узла
    temp->next = p;  // созданный узел указывает на следующий элемент
    return (temp);
}

struct node* remove_door(struct node* elem, struct node* root) {
    struct node* temp;
    temp = root;
    while (temp->next != elem)  // просматриваем список начиная с корня
    {                           // пока не найдем узел, предшествующий elem
        temp = temp->next;
    }
    temp->next = elem->next;  // переставляем указатель
    free(elem);               // освобождаем память удаляемого узла
    return (temp);
}

struct node* find_door(int door_id, struct node* root) {
    struct node* p;
    p = root;
    do {
        if ((p->val) == door_id) {
            break;
        }
        p = p->next;  // переход к следующему узлу
    } while (p != NULL);
    return p;
}

void destroy(struct node* root) {
    while (root != NULL) {
        struct node* temp;
        temp = root;
        root = root->next;
        free(temp);
    }
}