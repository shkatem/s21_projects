

#include <stdio.h>
#include <stdlib.h>

#include "door_struct.h"

struct node {
    int val;
    int status;
    struct node* next;
};

struct node* init(struct door* door);
struct node* add_door(struct node* elem, struct door* door);
struct node* find_door(int door_id, struct node* root);
struct node* remove_door(struct node* elem, struct node* root);
void destroy(struct node* root);

void add_door_test(int num);
void remove_door_test(const struct node* ptr);
