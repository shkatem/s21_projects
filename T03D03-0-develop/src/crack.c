
#include <stdio.h>

int main() {
    float x, y;
    scanf("%f%f", &x, &y);
    float sq_d = x * x + y * y;
    float sq_radius = 25.0;
    if (sq_d < sq_radius) {
        printf("GOTCHA");
    } else {
        printf("MISS");
    }
}
