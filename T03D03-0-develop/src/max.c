
#include <stdio.h>

int max(float a, float b);

int main() {
    float a, b;
    if (scanf("%f%f", &a, &b) != 2) {
        printf("You haven't provided 2 numbers. Try again");
        return 0;
    }
    if ((a != (int)a) || (b != (int)b)) {
        printf("n/a");
        return 0;
    }
    printf("%d", max(a, b));
    return 0;
}

int max(float a, float b) {
    float m = a;
    if (b > a) {
        m = b;
    }
    return m;
}
