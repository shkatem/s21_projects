
#include <math.h>
#include <stdio.h>

float the_function(float x);

int main() {
    float x;
    if (scanf("%f", &x) != 1) {
        printf("n/a");
        return 0;
    }

    if (x == 0) {
        printf("n/a");
        return 0;
    }
    float result = the_function(x);
    printf("%.1f", result);
    return 0;
}
float the_function(float x) {
    float y;
    y = (7e-3) * pow(x, 4.0);
    y += ((22.8 * pow(x, 1.0 / 3) - 1e3) * x + 3.0) / (x * x / 2.0);
    y += -x * pow((10.0 + x), (2.0 / x)) - 1.01;
    return y;
}
