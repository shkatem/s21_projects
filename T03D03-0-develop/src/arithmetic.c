
#include <stdio.h>

int sum(int a, int b);
int mul(int a, int b);
int min(int a, int b);
int div(int a, int b);

int main() {
    float a, b;
    if (scanf("%f%f", &a, &b) != 2) {
        printf("You haven't provided 2 integers. Try again");
        return 0;
    }
    if ((a != (int)a) || (b != (int)b)) {
        printf("You haven't provided 2 INTEGERS. Try again");
        return 0;
    }
    a = (int)a;
    b = (int)b;
    if (b == 0) {
        printf("%d %d %d %s", sum(a, b), min(a, b), mul(a, b), "n/a");
    } else {
        printf("%d %d %d %d", sum(a, b), min(a, b), mul(a, b), div(a, b));
    }
    return 0;
}

int sum(int a, int b) { return a + b; }

int mul(int a, int b) { return a * b; }

int min(int a, int b) { return a - b; }

int div(int a, int b) { return a / b; }
