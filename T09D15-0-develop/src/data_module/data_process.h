#ifndef PROCESSING_H
#define PROCESSING_H

#define EPS 1E-6

int normalization(double *data, int n);
void sort(double *a, int first, int last);
void swap(double *xp, double *yp);

#endif
