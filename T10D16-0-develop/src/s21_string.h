#define NULL __DARWIN_NULL
#include <stdio.h>

size_t s21_strlen(const char *str);
void s21_strlen_test();
int s21_strcmp(const char *str1, const char *str2);
void s21_strcmp_test();
char *s21_strcpy(char *str2, const char *str1);
void s21_strcpy_test();
char *s21_strcat(const char *str1, const char *str2);
void s21_strcat_test();
char *s21_strchr(const char *str, int n);
void s21_strchr_test();
char *s21_strstr(const char *str1, const char *str2);
void s21_strstr_test();