#include "s21_string.h"

#include <stdlib.h>

int main() {
#ifdef _STRLEN
    s21_strlen_test();
#endif
#ifdef _STRCMP
    s21_strcmp_test();
#endif
#ifdef _STRCPY
    s21_strcpy_test();
#endif
#ifdef _STRCAT
    s21_strcat_test();
#endif
#ifdef _STRCHR
    s21_strchr_test();
#endif
#ifdef _STRSTR
    s21_strstr_test();
#endif
#ifdef _STRTOK
    s21_strtok_test();
#endif

    return 1;
}

void s21_strlen_test() {
    const char *str1 = "qwertyuio";
    const char *str2 = "";
    const char *str3 = "0";
    // test 1
    printf("%s\n", str1);
    printf("%ld\n", s21_strlen(str1));
    if (s21_strlen(str1) == 9) {
        printf("SUCCESS\n");
    } else {
        printf("FAIL\n");
    }
    // test 2
    printf("%s\n", str2);
    printf("%ld\n", s21_strlen(str2));
    if (s21_strlen(str2) == 0) {
        printf("SUCCESS\n");
    } else {
        printf("FAIL\n");
    }
    // test 3
    printf("%s\n", str3);
    printf("%ld\n", s21_strlen(str3));
    if (s21_strlen(str3) == 1) {
        printf("SUCCESS\n");
    } else {
        printf("FAIL\n");
    }
}

void s21_strcmp_test() {
    const char *str1 = "1234567890";
    const char *str2 = "123456789";
    // test 1
    printf("%s\n%s\n", str1, str2);
    printf("%d\n", s21_strcmp(str1, str2));
    if (s21_strcmp(str1, str2) > 0) {
        printf("SUCCESS\n");
    } else {
        printf("FAIL\n");
    }
    const char *str3 = "qwer";
    const char *str4 = "qwerty";
    // test 2
    printf("%s\n%s\n", str3, str4);
    printf("%d\n", s21_strcmp(str3, str4));
    if (s21_strcmp(str3, str4) < 0) {
        printf("SUCCESS\n");
    } else {
        printf("FAIL\n");
    }
    // test 3
    const char *str5 = "";
    const char *str6 = "";
    printf("%s\n%s\n", str5, str6);
    printf("%d\n", s21_strcmp(str5, str6));
    if (s21_strcmp(str5, str6) == 0) {
        printf("SUCCESS\n");
    } else {
        printf("FAIL\n");
    }
}

void s21_strcpy_test() {
    const char *str1 = "1234567890";
    char str2[10] = "";
    printf("%s\n", str1);
    printf("%s\n", s21_strcpy(str2, str1));
    if (*s21_strcpy(str2, str1) == *str1) {
        printf("SUCCESS\n");
    } else {
        printf("FAIL\n");
    }
    const char *str3 = "";
    char str4[] = "";
    printf("%s\n", str3);
    printf("%s\n", s21_strcpy(str4, str3));
    if (*s21_strcpy(str4, str3) == *str3) {
        printf("SUCCESS\n");
    } else {
        printf("FAIL\n");
    }
    const char *str5 = "qwerty";
    char str6[6] = "1";
    printf("%s\n", str5);
    printf("%s\n", s21_strcpy(str6, str5));
    if (*s21_strcpy(str6, str5) == *str5) {
        printf("SUCCESS\n");
    } else {
        printf("FAIL\n");
    }
}

void s21_strcat_test() {
    const char *str1 = "1234567890";
    const char *str2 = "qwerty";
    printf("%s\n%s\n", str1, str2);
    printf("%s\n", s21_strcat(str1, str2));
    if (s21_strcmp(s21_strcat(str1, str2), "qwerty1234567890") == 0) {
        printf("SUCCESS\n");
    } else {
        printf("FAIL\n");
    }
    const char *str3 = "";
    const char *str4 = "";
    printf("%s\n%s\n", str3, str4);
    printf("%s\n", s21_strcat(str3, str4));
    if (s21_strcmp(s21_strcat(str3, str4), "") == 0) {
        printf("SUCCESS\n");
    } else {
        printf("FAIL\n");
    }
    const char *str5 = " qwery ";
    const char *str6 = "";
    printf("%s\n%s\n", str5, str6);
    printf("%s\n", s21_strcat(str5, str6));
    if (s21_strcmp(s21_strcat(str5, str6), " qwery ") == 0) {
        printf("SUCCESS\n");
    } else {
        printf("FAIL\n");
    }
}

void s21_strchr_test() {
    const char *str1 = "1234567890";
    int n1 = '6';
    printf("%s\n", str1);
    printf("%s\n", s21_strchr(str1, n1));
    if (s21_strchr(str1, n1) == &str1[5]) {
        printf("SUCCESS\n");
    } else {
        printf("FAIL\n");
    }
    const char *str2 = "qwerty";
    int n2 = 'a';
    printf("%s\n", str2);
    printf("%s\n", s21_strchr(str2, n2));
    if (s21_strchr(str2, n2) == NULL) {
        printf("SUCCESS\n");
    } else {
        printf("FAIL\n");
    }
    const char *str3 = "";
    int n3 = ' ';
    printf("%s\n", str3);
    printf("%s\n", s21_strchr(str3, n3));
    if (s21_strchr(str3, n3) == NULL) {
        printf("SUCCESS\n");
    } else {
        printf("FAIL\n");
    }
}

void s21_strstr_test() {
    const char *str1 = "1234567890";
    const char *str2 = "678";
    printf("%s\n%s\n", str1, str2);
    printf("%s\n", s21_strstr(str1, str2));
    if (s21_strstr(str1, str2) == &str1[5]) {
        printf("SUCCESS\n");
    } else {
        printf("FAIL\n");
    }
    const char *str3 = "678";
    const char *str4 = "1234567890";
    printf("%s\n%s\n", str3, str4);
    printf("%s\n", s21_strstr(str3, str4));
    if (s21_strstr(str3, str4) == NULL) {
        printf("SUCCESS\n");
    } else {
        printf("FAIL\n");
    }
    const char *str5 = "";
    const char *str6 = "";
    printf("%s\n%s\n", str5, str6);
    printf("%s\n", s21_strstr(str5, str6));
    if (s21_strcmp(s21_strstr(str5, str6), "") == 0) {
        printf("SUCCESS\n");
    } else {
        printf("FAIL\n");
    }
}