#include "s21_string.h"

#include <stdio.h>
#include <stdlib.h>

size_t s21_strlen(const char *str) {
    size_t length = 0;
    for (; *(str + length); length++)
        ;
    return length;
}

int s21_strcmp(const char *str1, const char *str2) {
    for (; *str1 && (*str1 == *str2); str1++, str2++)
        ;
    return *str1 - *str2;
}

char *s21_strcpy(char *str2, const char *str1) {
    size_t length = 0;
    for (; str1[length] != '\0'; length++) {
        str2[length] = str1[length];
    }
    str2[length] = '\0';
    return str2;
}

char *s21_strcat(const char *str1, const char *str2) {
    char *temp = (char *)malloc((s21_strlen(str1) + s21_strlen(str2)) * sizeof(char));
    size_t length1 = 0;
    size_t length2 = 0;
    for (; str2[length2] != '\0'; length2++) {
        temp[length2] = str2[length2];
    }
    for (; str1[length1] != '\0'; length1++) {
        temp[length1 + length2] = str1[length1];
    }
    temp[s21_strlen(str1) + s21_strlen(str2)] = '\0';
    free(temp);
    return temp;
}

char *s21_strchr(const char *str, int n) {
    size_t length = 0;
    int q = 0;
    char *result = NULL;
    for (; str[length] != '\0'; length++) {
        if (!q) {
            if ((int)str[length] == n) {
                q = 1;
                result = ((char *)str + length);
                break;
            }
        };
    }
    if (!q) {
        result = NULL;
    }
    return result;
}

char *s21_strstr(const char *str1, const char *str2) {
    char *result = NULL;
    char *a, *b;
    int q = 1;

    b = (char *)str2;
    if (*b == 0) {
        result = (char *)str1;
        q = 0;
    }
    if (q) {
        for (; *str1 != 0; str1 += 1) {
            if (*str1 != *b) {
                continue;
            }
            a = (char *)str1;
            while (1) {
                if (*b == 0) {
                    result = (char *)str1;
                }
                if (*a++ != *b++) {
                    break;
                }
            }
            b = (char *)str2;
        }
    }
    return result;
}
