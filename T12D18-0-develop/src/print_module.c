#include "print_module.h"

#include <stdio.h>
#include <time.h>

char print_char(char ch) { return putchar(ch); }

void print_log(char (*print)(char), char* message) {
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    printf("%s %02d:%02d:%02d ", Log_prefix, tm.tm_hour, tm.tm_min, tm.tm_sec);
    for (; *message; message++) {
        (*print)(*message);
    }
}