#include "bst.h"

#include <stdio.h>
#include <stdlib.h>

t_btree *bstree_create_node(int item) {
    t_btree *head = (t_btree *)malloc(sizeof(t_btree));
    head->item = item;
    head->left = NULL;
    head->right = NULL;
    return head;
}

void bstree_insert(t_btree *root, int item, int (*cmpf)(int, int)) {
    if ((*cmpf)(root->item, item))
        if (root->left)
            bstree_insert(root->left, item, compare);
        else
            root->left = bstree_create_node(item);
    else if (root->right)
        bstree_insert(root->right, item, compare);
    else
        root->right = bstree_create_node(item);
}

int compare(int a, int b) {
    int q = 0;
    if (a > b) {
        q = 1;
    }
    return q;
}

void destroy(t_btree *root) {
    if (root != NULL) {
        destroy(root->left);
        destroy(root->right);
        free(root);
    } else {
        return;
    }
}

void bstree_apply_infix(t_btree *root, void (*applyf)(int)) {
    if (root == NULL) {
        return;
    }
    bstree_apply_infix(root->left, applyf);
    (*applyf)(root->item);
    bstree_apply_infix(root->right, applyf);
}

void bstree_apply_prefix(t_btree *root, void (*applyf)(int)) {
    if (root == NULL) {
        return;
    }
    (*applyf)(root->item);
    bstree_apply_prefix(root->left, applyf);
    bstree_apply_prefix(root->right, applyf);
}

void bstree_apply_postfix(t_btree *root, void (*applyf)(int)) {
    if (root == NULL) {
        return;
    }
    bstree_apply_prefix(root->left, applyf);
    bstree_apply_prefix(root->right, applyf);
    (*applyf)(root->item);
}

void applyf(int a) { printf("%d ", a); }