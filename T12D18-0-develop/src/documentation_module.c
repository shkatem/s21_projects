#include "documentation_module.h"

#include <stdarg.h>
#include <stdlib.h>

int validate(char* data) {
    int validation_result = !strcmp(data, Available_document);
    return validation_result;
}

int* check_available_documentation_module(int (*validate)(char*), int document_count, ...) {
    int* mask;
    mask = (int*)malloc(document_count * sizeof(int));
    va_list ptr;

    // Initializing argument to the
    // list pointer
    va_start(ptr, document_count);
    for (int i = 0; i < document_count; i++) {
        // Accessing current variable
        // and pointing to next one
        mask[i] = (*validate)(va_arg(ptr, char*));
    }

    // Ending argument list traversal
    va_end(ptr);
    return mask;
}

void output(int* mask, int document_count, ...) {
    va_list ptr;
    va_start(ptr, document_count);
    for (int i = 0; i < document_count; i++) {
        char* avail;
        if (mask[i] == 1) {
            avail = "available";
        } else {
            avail = "unavailable";
        }
        printf("%.15s: %s\n", va_arg(ptr, char*), avail);
    }
    va_end(ptr);
}
