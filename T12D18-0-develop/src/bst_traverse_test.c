#include <stdio.h>
#include <stdlib.h>

#include "bst.h"

int main() {
    t_btree *a, *b, *c;
    a = bstree_create_node(5);
    b = bstree_create_node(2);
    c = bstree_create_node(3);
    bstree_insert(a, 3, compare);
    bstree_insert(a, 7, compare);
    bstree_apply_infix(a, applyf);
    printf("\n");
    bstree_apply_prefix(a, applyf);
    printf("\n");
    bstree_apply_postfix(a, applyf);
    destroy(a);
    free(b);
    free(c);
}