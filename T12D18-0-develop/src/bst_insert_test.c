#include <stdio.h>
#include <stdlib.h>

#include "bst.h"

int main() {
    t_btree *a, *b, *c;
    a = bstree_create_node(5);
    b = bstree_create_node(2);
    c = bstree_create_node(3);
    bstree_insert(a, 3, compare);
    bstree_insert(a, 7, compare);
    printf("%d %d %d", (a->left)->item, (a->right)->item, a->item);
    destroy(a);
    free(b);
    free(c);
}