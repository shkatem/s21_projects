#include <stdio.h>
#include <stdlib.h>

#include "bst.h"

int main() {
    t_btree *a, *b, *c;
    a = bstree_create_node(5);
    b = bstree_create_node(2);
    c = bstree_create_node(3);
    bstree_insert(a, 3, compare);
    bstree_insert(a, 7, compare);
    free(a);
    free(b);
    free(c);
}