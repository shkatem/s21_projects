#include <stdio.h>

#include "database.h"

ENTITY *select_status(FILE *db, int id) { return select(db, id); }

int delete_status(FILE *db, int id, char *s) { return delete (db, id, s); }

int insert_status(FILE *db, ENTITY *entity) { return insert(db, entity); }

int update_status(FILE *db, int id, ENTITY *entity, char *s) { return update(db, id, entity, s); }

int print_status(ENTITY *arr) {
    printf("%d %d %d %s %s\n", (*arr).id, (*arr).module_id, (*arr).module_status, (*arr).date, (*arr).time);
    return 0;
}