#include <stdio.h>

#include "database.h"

ENTITY *select_modules(FILE *db, int id) { return select(db, id); }

int delete_modules(FILE *db, int id, char *s) { return delete (db, id, s); }

int insert_modules(FILE *db, ENTITY *entity) { return insert(db, entity); }

int update_modules(FILE *db, int id, ENTITY *entity, char *s) { return update(db, id, entity, s); }

int print_modules(ENTITY *arr) {
    printf("%d %s %d %d %d\n", (*arr).id, (*arr).name, (*arr).memory_location, (*arr).cells, (*arr).flag);
    return 0;
}

int data_ins_modules(ENTITY *arr) {
    char b;
    char ch = 'x';
    int i = 0;
    scanf("%d%c", &(*arr).id, &b);
    while ((ch != '\n') && (ch != ' ') && (i < 30)) {
        scanf("%c", &ch);
        ((*arr).name)[i] = ch;
        i++;
    }
    scanf("%d%c", &(*arr).memory_location, &b);
    scanf("%d%c", &(*arr).cells, &b);
    scanf("%d%c", &(*arr).flag, &b);
    return 0;
}