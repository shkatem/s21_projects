#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "levels.h"
#include "modules.h"
#include "status_events.h"
int get_operation();

int main() {
    int o = -1, v = -1, i = -1;
    FILE *f;
    MODULES *m;
    LEVELS *l;
    STATUS_EVENTS *s;
    char *sm = "../materials/master_modules.db";
    char *sl = "../materials/master_levels.db";
    char *ss = "../materials/master_status_events.db";
    printf(
        "Please choose one operation:\n1. SELECT\n2. INSERT\n3. UPDATE\n4. DELETE\n5. Get all active "
        "additional modules (last module status is 1)\n6. Delete modules by ids\n7. Set protected mode for "
        "module by id\n8. Move module by id to specified memory level and cell\n9. Set protection flag of "
        "the specified memory level\n");
    o = get_operation();
    printf("Please choose a table:\n1. Modules\n2. Levels\n3. Status events\n");
    v = get_operation();
    switch (v) {
        case 1:
            f = fopen(sm, "rb+");
            break;
        case 2:
            f = fopen(sl, "rb+");
            break;
        case 3:
            f = fopen(ss, "rb+");
            break;
        default:
            printf("n/a");
    }

    switch (o) {
        case 1:
            printf("Insert the id of the record you want: ");
            scanf("%d", &i);
            if (v == 1) {
                m = select_modules(f, i);
                print_modules(m);
                free(m);
            } else if (v == 2) {
                l = select_levels(f, i);
                print_levels(l);
                free(l);
            } else if (v == 3) {
                s = select_status(f, i);
                print_status(s);
                free(s);
            } else {
                printf("n/a");
            }
            break;
        case 2:
            printf("Insert the data: ");
            if (v == 1) {
                m = (MODULES *)malloc(sizeof(MODULES *));
                data_ins_modules(m);
                insert_modules(f, m);
                free(m);
            } else if (v == 2) {
                l = (LEVELS *)malloc(sizeof(LEVELS *));
                data_ins_levels(l);
                insert_levels(f, l);
                free(l);
            }
            // else if (v == 3) {s = select_status(f,i); delete_status(f,i,ss); print_status(s); free(s);}
            else {
                printf("n/a");
            }
            break;
        case 3:
            printf("Insert new id and then entity to update: ");
            if (v == 1) {
                m = (MODULES *)malloc(sizeof(MODULES *));
                data_ins_modules(m);
                update_modules(f, (*m).id, m, sm);
                free(m);
            } else if (v == 2) {
                l = (LEVELS *)malloc(sizeof(LEVELS *));
                data_ins_levels(l);
                update_levels(f, (*l).id, l, sl);
                free(l);
            }
            // else if (v == 3) {s = select_status(f,i); delete_status(f,i,ss); print_status(s); free(s);}
            else {
                printf("n/a");
            }
            break;
        case 4:
            printf("Insert the id of the record you want to delete: ");
            scanf("%d", &i);
            if (v == 1) {
                delete_modules(f, i, sm);
            } else if (v == 2) {
                delete_levels(f, i, sl);
            } else if (v == 3) {
                delete_status(f, i, ss);
            } else {
                printf("n/a");
            }
            break;
        default:
            printf("n/a");
    }
    // printf("%d", c);
    // ENTITY* arr = select(f, 19);
    // printf("%d %s %d %d %d", arr[0].id, arr[0].name, arr[0].memory_location, arr[0].cells, arr[0].flag);
    // free(arr);
    fclose(f);

    // free(s);
    return 1;
}

int get_operation() {
    int variant;
    char b;
    int m;
    m = scanf("%d%c", &variant, &b);
    if (m != 2 || ((variant != 1) && (variant != 2) && (variant != 3) && (variant != 4) && (variant != 5) &&
                   (variant != 6) && (variant != 7))) {
        printf("n/a");
        variant = -1;
    }
    return variant;
}