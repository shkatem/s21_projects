// #define ENTITY struct MODULES
#include <stdio.h>
#include <stdlib.h>
typedef struct LEVELS {
    int id;
    int cells;
    int flag;
} LEVELS;

typedef struct STATUS_EVENTS {
    int id;
    int module_id;
    int module_status;
    char date[11];
    char time[9];
} STATUS_EVENTS;

typedef struct MODULES {
    int id;
    char name[30];
    int memory_location;
    int cells;
    int flag;
} MODULES;

//////////////////////////////////////////////////////
ENTITY *select(FILE *db, int id);
int delete(FILE *db, int id, char* s);
int insert(FILE *db, ENTITY *entity);
int update(FILE *db, int id, ENTITY *entity, char* s);
//////////////////////////////////////////////////////
ENTITY read_record_from_file(FILE *pfile, int index);
int get_records_count_in_file(FILE *pfile);
int get_file_size_in_bytes(FILE *pfile);
void write_record_in_file(FILE *pfile, const ENTITY *record_to_write);
void print_first_module();


