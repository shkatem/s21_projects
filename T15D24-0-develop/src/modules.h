typedef struct MODULES {
    int id;
    char name[30];
    int memory_location;
    int cells;
    int flag;
} MODULES;

//////////////////////////////////////////////////////
MODULES *select_modules(FILE *db, int id);
int delete_modules(FILE *db, int id, char* s);
int insert_modules(FILE *db, MODULES *entity);
int update_modules(FILE *db, int id, MODULES *entity, char* s);
int print_modules(MODULES * arr);
int data_ins_modules(MODULES * arr);