typedef struct STATUS_EVENTS {
    int id;
    int module_id;
    int module_status;
    char date[11];
    char time[9];
} STATUS_EVENTS;

//////////////////////////////////////////////////////
STATUS_EVENTS *select_status(FILE *db, int id);
int delete_status(FILE *db, int id, char* s);
int insert_status(FILE *db, STATUS_EVENTS *entity);
int update_status(FILE *db, int id, STATUS_EVENTS *entity, char* s);
int print_status(STATUS_EVENTS * arr);