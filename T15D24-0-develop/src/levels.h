typedef struct LEVELS {
    int id;
    int cells;
    int flag;
} LEVELS;

//////////////////////////////////////////////////////
LEVELS *select_levels(FILE *db, int id);
int delete_levels(FILE *db, int id, char* s);
int insert_levels(FILE *db, LEVELS *entity);
int update_levels(FILE *db, int id, LEVELS *entity, char* s);
int print_levels(LEVELS * arr);
int data_ins_levels(LEVELS * arr);