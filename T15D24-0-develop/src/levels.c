#include <stdio.h>

#include "database.h"

ENTITY *select_levels(FILE *db, int id) { return select(db, id); }

int delete_levels(FILE *db, int id, char *s) { return delete (db, id, s); }

int insert_levels(FILE *db, ENTITY *entity) { return insert(db, entity); }

int update_levels(FILE *db, int id, ENTITY *entity, char *s) { return update(db, id, entity, s); }

int print_levels(ENTITY *arr) {
    printf("%d %d %d\n", (*arr).id, (*arr).cells, (*arr).flag);
    return 0;
}

int data_ins_levels(ENTITY *arr) {
    char b;
    scanf("%d%c", &(*arr).id, &b);
    scanf("%d%c", &(*arr).cells, &b);
    scanf("%d%c", &(*arr).flag, &b);
    return 0;
}